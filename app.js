// SAGE2 is available for use under the SAGE2 Software License
//
// University of Illinois at Chicago's Electronic Visualization Laboratory (EVL)
// and University of Hawai'i at Manoa's Laboratory for Advanced Visualization
// and Applications (LAVA)
//
// See full text, terms and conditions in the LICENSE.txt included file
//
// Copyright (c) 2017

// Node.js Login Boilerplate
// More Info : http://kitchen.braitsch.io/building-a-login-system-in-node-js-and-mongodb/
// Copyright (c) 2013-2016 Stephen Braitsch

// XXX - VERY VERY DANGEROUS - NEVER ENABLE
//process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

var http = require('http');
var https = require('https');
var express = require('express');
var session = require('express-session');
var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');
var mongoose = require('mongoose');
var MongoStore = require('connect-mongo')(session);
var fs = require('fs');
var path = require('path');
var config = require('./config');
// var errorHandler    = require('errorhandler');

// Build the Express application
var app = express();

// Load the certificates for HTTPS
var certs = {
	ca: fs.readFileSync('certs/' + config.certs.ca),
	key: fs.readFileSync('certs/' + config.certs.key),
	cert: fs.readFileSync('certs/' + config.certs.cert)
};

if (app.get('env') === 'development') {
	var livereload = require('livereload');
	var liveServer = livereload.createServer({
		https: {
			cert: certs.cert,
			key: certs.key
		},
		exts: ['pug', 'styl']
	});
	liveServer.watch([
		path.join(__dirname, 'app', 'server', 'views'),
		path.join(__dirname, 'app', 'public')
		]);

}


// Set Express parameters
app.locals.pretty = true;
app.set('port', config.https);
app.set('views', __dirname + '/app/server/views');
app.set('view engine', 'pug');
app.use(cookieParser());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(require('stylus').middleware({ src: __dirname + '/app/public' }));
app.use(express.static(__dirname + '/app/public'));


if(config.useMongoAuthentication) {
	require('./app/server/modules/database-manager')(config.mongoHost, config.mongoPort, config.mongoName, config.mongoUser, config.mongoPass);
} else {
	require('./app/server/modules/database-manager')(config.mongoHost, config.mongoPort, config.mongoName);
}

app.use(session({
	secret: 'faeb4453e5d14fe6f6d04637f78077c76c73d1b4',
	proxy: true,
	resave: true,
	saveUninitialized: true,
	store: new MongoStore({mongooseConnection: mongoose.connection})
}));

// Setup the HTTP routes
require('./app/server/routes')(app);

// Create the HTTP web server to redirect to HTTPS
http.createServer(function (req, res) {
	var host = req.headers.host.replace(config.http, config.https);
	res.writeHead(301, { Location: 'https://' + host + req.url });
	res.end();
}).listen(config.http);

// Create a HTTPS server and link it to the Express app
var server = https.createServer(certs, app);

var io = require('socket.io')(server);
var nsp = io.of('/docker');
nsp.on('connection', function(socket){

	socket.on('containerLoading', function(o){
		socket.emit('containerUpdate', o);
	});

	socket.on('registerClient', function(userid) {
		socket.join(userid);
	});
});

app.set('socketio', nsp);

server.listen(app.get('port'), function () {
	console.log('Express server listening on port ' + app.get('port') - 1);
});


