# A server for creating SAGE2 docker containers 
[![Coverage Status](https://coveralls.io/repos/bitbucket/sage2/sage2cloud/badge.svg)](https://coveralls.io/bitbucket/sage2/sage2cloud)

# Introduction
sage2cloud is a management system for `SAGE2` servers. With sage2cloud you can:

* Create, deploy and configure SAGE2 local and remote servers
* Edit and customize the configuration
* Start/Stop the SAGE2 servers

# Prerequisites
* `docker`
* `mongodb`

# Recommended
Install `pm2` to manage the nodejs instance

```
#!bash
npm install pm2@latest -g
```

# Installation
Add the user that will be running docker into the docker group:
```
#!bash
sudo vi /etc/group
```
Run `docker ps` to make sure you can connect to docker

Make sure docker daemon is running: on a machine with systemctl, use:
```
systemctl status docker
```

Clone the repository:
```
#!bash
git clone https://bitbucket.org/sage2/sage2cloud
cd sage2cloud
mkdir certs
npm run in
```

For sage2cloud development instances, you can disable mongodb authentication by editing '/etc/mongodb.conf' and commenting out the lines starting with 'security' and 'authorization'. If you have set up mongodb with authentication, then make sure you provide the right credentials in config.js (see below).

Make sure that 'mongodb' is running: on a machine with systemctl, use:
```
systemctl status mongodb
```


## Edit configuration file
```
#!bash
cp config.js.example config.js
vi config.js
```

* `hostname`: The domain where sage2cloud is running. For example, www.sage2cloud.com. Make sure you have the DNS set up correctly.
* `http`: The http port that sage2cloud will be running.
* `https`: The https port that sage2cloud will be running.
* `mongoHost`: URL where mongodb is running. If your mongodb is running on the same machine as sage2cloud, leave it as `localhost`. If you are not sure, leave it as is.
* `mongoPort`: Port where mongodb is running. Default is 27017. If you are unsure, leave it as is.
* `mongoName`: Name of the db in mongodb. For most instances, `sage2Cloud` should work.
* `mongoUser`: If mongodb authentication is enabled, here you have to enter the username. If not, leave it as is.
* `mongoPass`: If mongodb authentication is enabled, here you have to enter the password. If not, leave it as is.
* `useMongoAuthentication`: `true` if authentication is enabled (don't forget to set username and password above), `false` for no authentication.
* `certs`:  Ca, cert and key certificates. These files should be in the certs directory. See below for instructions on creating certificates if you don't have any.

## Set up the SSL certificate
Copy certs in the certs directory. NOTE: Certificates need to follow the following naming convention:
```
#!bash
_.<domain>-ca.crt
_.<domain>.crt
_.<domain>.key
```

### If you don't have a certificate, then you can generate a self-signed one (Linux only instructions):
```
#!bash
wget https://bitbucket.org/sage2/sage2/raw/8f4b44f1e24068a24cba8f4462838bf6adbfdf39/keys/init_webserver.sh
chmod 755 init_webserver.sh
./init_webserver.sh <ip or hostname>
```

For example, if you are setting up sage2cloud for google.com, then your keys should be named as follows:
```
#!bash
_.google.com-ca.crt
_.google.com.crt
_.google.com.key
```

## pm2 cheat-sheet 
Start app
```
#!bash
pm2 start app.js
```

Stop app
```
#!bash
pm2 stop app.js
```

Add it to the startup
```
#!bash
sudo pm2 startup systemd
```

Run as sage user
```
#!bash
sudo pm2 startup -u sage
```

Reload pm2 (start/stop the pm2 daemon with all the apps that's controlling)
```
#!bash
sudo pm2 update
```
## Credits

Account management backbone forked from [Node-Login](https://github.com/braitsch/node-login) created by Stephen Braitsch.

##### Notice #####
SAGE and SAGE2 are trademarks of the University of Illinois Board of Trustees (SAGE� and SAGE2�).
