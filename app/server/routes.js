// SAGE2 is available for use under the SAGE2 Software License
//
// University of Illinois at Chicago's Electronic Visualization Laboratory (EVL)
// and University of Hawai'i at Manoa's Laboratory for Advanced Visualization
// and Applications (LAVA)
//
// See full text, terms and conditions in the LICENSE.txt included file
//
// Copyright (c) 2017

var async	= require('async');
var config	= require('../../config');
var moment	= require('moment-timezone');
var AM		= require('./modules/account-manager');
var EM		= require('./modules/email-dispatcher');
var SM		= require('./modules/server-manager');
var DM		= require('./modules/docker-manager');
var SC		= require('./modules/server-controller');

var containerRow = require('pug').compileFile(__dirname + '/views/containerRow.pug');
var dockerRow = require('pug').compileFile(__dirname + '/views/dockerRow.pug');

module.exports = function (app) {
	// main login page
	app.get('/', function (req, res) {
		// check if the user's credentials are saved in a cookie
		if (req.cookies.user == undefined || req.cookies.pass == undefined) {
			res.render('login', { title: 'Hello - Please Login To Your Account' });
		} else {
			// attempt automatic login
			AM.autoLogin(req.cookies.user, req.cookies.pass, function (o) {
				if (o != null) {
					req.session.user = o;
					res.redirect('/home');
				} else {
					res.render('login', { title: 'Hello - Please Login To Your Account' });
				}
			});
		}
	});

	app.post('/', function (req, res) {
		AM.manualLogin(req.body.user, req.body.pass, function (e, o) {
			if (!o) {
				res.status(400).send(e);
			} else {
				req.session.user = o;
				if (req.body['remember-me'] == 'true') {
					res.cookie('user', o.user, { maxAge: 900000 });
					res.cookie('pass', o.pass, { maxAge: 900000 });
				}
				res.status(200).send(o);
			}
		});
	});

	// logged-in user homepage
	app.get('/home', function (req, res) {
		if (req.session.user == null) {
			// if user is not logged-in redirect back to login page
			res.redirect('/');
		} else {
			async.parallel([
				function(callback) {
					AM.isAdmin(req.session.user._id, callback);
				},
				function(callback) {
					SC.getServersByUserId(req.session.user._id, callback);
				},
				function(callback) {
					DM.getDockerManagers(callback);
				},
				function(callback) {
					DM.getDockerImages(callback);
				},
				function(callback) {
					SC.checkForUpdates(req.session.user._id, callback);
				}
			], function(error, results) {
				if (error) {
					// XXX
				} else {
					res.render('home', {
						title: 'Control Panel',
						udata: req.session.user,
						servers: results[1],
						admin: results[0].admin,
						dockerServers: results[2],
						localDockerServer: config.hostname,
						adminPage: false,
						images: results[3],
						update: results[4],
						timezones: moment.tz.names(),
						user: req.session.user
					});
				}
			});
		}
	});

	app.get('/admin', function (req, res) {
		if (req.session.user == null) {
			// if user is not logged-in redirect back to login page
			res.redirect('/');
		} else {
			async.parallel([
				function(callback) {
					AM.isAdmin(req.session.user._id, callback);
				},
				function(callback) {
					SC.getAllServersUsersDockers(callback);
				},
				function(callback) {
					DM.getDockerManagers(callback);
				},
				function(callback) {
					SC.checkForUpdatesAllServers(callback);
				},
				function(callback) {
					DM.getDockerImages(callback);
				}

			], function(error, results) {
				if (results[0].admin) {
					res.render('admin', {
						title: 'Control Panel',
						udata: req.session.user,
						servers: results[1].servers,
						accounts: results[1].accounts,
						dockerManagers: results[1].dockerManagers,
						admin: results[0].admin,
						dockerServers: results[2],
						localDockerServer: config.hostname,
						adminPage: true,
						update: results[3],
						user: req.session.user,
						images: results[4],
						timezones: moment.tz.names()
					});
				} else {
					res.redirect('/home');
				}
			});

			/*
			// Check if user is admin
			AM.isAdmin(req.session.user._id, function (e, user) {
				if (e) {
					// nothing
				} else {
					if (user.admin) {
						SC.getAllServersUsersDockers(function (e, o) {
							res.render('admin', {
								title: 'Control Panel',
								udata: req.session.user,
								servers: o.servers,
								accounts: o.accounts,
								dockerManagers: o.dockerManagers,
								admin: user.admin
							});
						});
					} else {
						res.redirect('/home');
					}
				}
			});*/
		}
	});

	app.post('/admin', function (req, res) {
		if (req.session.user == null) {
			res.redirect('/');
		} else {
			// Check if user is admin
			AM.isAdmin(req.session.user._id, function (e, user) {
				if (e) {
					// nothing
				} else {
					if (req.body.action != null) {
						if (req.body.action == 'deleteAccount') {
							console.log('Attempting to delete Account');
							SC.deleteAccount(req.body.accountId, function (e, o) {
								var io = app.get('socketio');
								io.to('admin').emit('removeAccount', {id: req.body.accountId});
								res.status(200).send('ok');
							});
						}
					}
				}
			});
		}
	});

	app.post('/account', function (req, res) {
		if (req.session.user == null) {
			res.redirect('/');
		} else {
			AM.isAdmin(req.session.user._id, function (e, user) {
				var id = req.session.user._id;
				if (user.admin && req.query.id != undefined) {
					id = req.query.id;
				}
				var admin = 'off';
				if (req.body.admin === 'on') {
					admin = 'on';
				}
				AM.updateAccount({
					id: id,
					name: req.body.name,
					email: req.body.email,
					pass: req.body.pass,
					admin: admin
				}, function (e, o) {
					if (e) {
						res.status(400).send('error-updating-account');
					} else {
						// req.session.user = o;
						// update the user's login cookies if they exists
						// if (req.cookies.user != undefined && req.cookies.pass != undefined){
						//	res.cookie('user', o.user, { maxAge: 900000 });
						//	res.cookie('pass', o.pass, { maxAge: 900000 });
						// }
						res.status(200).send('ok');
					}
				});
			});
		}
	});

	app.post('/home', function (req, res) {
		if (req.session.user == null) {
			res.redirect('/');
		} else {
			if (req.body.action != null) {
				if (req.body.action == 'deleteServer') {
					SC.deleteServer(req.body.serverId,
						function (e, o) {
							if (e) {
								res.status(400).send(e);
							} else {
								var io = app.get('socketio');
								io.to('admin').emit('containerDelete', {id: req.body.serverId, status: 'Running'});
								io.to(req.session.user._id).emit('containerDelete', {id: req.body.serverId, status: 'Running'});
								res.status(200).send('ok');
							}
						});
				} else if (req.body.action === 'startServer') {
					SC.startServer(req.body.serverId,
						function (e, d) {
							if (e) {
								res.status(400).send(e);
							} else {
								var io = app.get('socketio');
								io.to('admin').emit('containerUpdate', {id: req.body.serverId, status: 'Running'});
								io.to(req.session.user._id).emit('containerUpdate', {id: req.body.serverId, status: 'Running'});
								res.status(200).send('ok');
							}
						});
				} else if (req.body.action === 'stopServer') {
					SC.stopServer(req.body.serverId,
						function (e, d) {
							var io = app.get('socketio');
							io.to('admin').emit('containerUpdate', {id: req.body.serverId, status: 'Stopped'});
							io.to(req.session.user._id).emit('containerUpdate', {id: req.body.serverId, status: 'Stopped'});
							res.status(200).send('ok');
						});
				}
			} else {
				AM.isAdmin(req.session.user._id, function (e, user) {
					var server = {
						name: req.body.name,
						user: req.session.user.user,
						userId: req.session.user._id,
						conf: req.body.conf,
						docker: req.body.docker,
						tag: req.body.tag,
						timezone: req.body.timezone
					};

					if (user.admin && req.body.owner !== undefined && req.body.ownerId !== undefined) {
						server.userId = req.body.ownerId;
						server.user = req.body.owner;
					}

					SC.addServer(server, function (e, server) {
						if (e) {
							res.status(400).send(e);
						} else {
							server.state = 'Stopped';
							server.admin = user.admin;
							server.adminPage = false;
							var update = [];
							update[server._id] = 'no-update';
							var row = containerRow({server: server, update: update});
							//pug1.render()
							var io = app.get('socketio');
							io.to('admin').emit('containerAdd', {row: row});
							io.to(req.session.user._id).emit('containerAdd', {row: row});
							res.status(200).send('ok');
						}
					});
				});
			}
		}
	});

	app.post('/container/updateConf', function (req, res) {
		var server = req.body.server;
		server.user = req.session.user.user;
		server.userId = req.session.user._id;
		server._id = server.id;


		SC.updateServer(server, function (error) {
			if (!error) {
				//AM.isAdmin(req.session.user._id, function(e, o) {
				//	if(!e) {
						var io = app.get('socketio');
						server.state = "Stopped";
						var update = [];
						update[server._id] = 'no-update';
						var row = containerRow({server: server, update: update});
						var adminRow = containerRow({server: server, update: update, admin: true, adminPage: true});
						var io = app.get('socketio');
						io.to('admin').emit('containerUpdateConf', {id: server.id, row: adminRow});
						io.to(req.session.user._id).emit('containerUpdateConf', {id: server.id, row: row});
						res.status(200).send('ok');
				//	} else {
				//		res.status(400).send(error);
				//	}

				//});
			} else {
				res.status(400).send(error);
			}
		});
	});

	app.post('/container/checkStatus', function(req, res) {
		SC.getContainerStatus(req.body.serverId, function(error, server) {
			if (!error) {
				res.status(200).send({state: server.state});
			} else {
				res.status(400).send(error);
			}
		});
	});

	app.post('/container/update', function(req, res) {
		SC.updateContainer(req.body.id, function(error, server) {
			if(!error) {
				var io = app.get('socketio');
				io.to('admin').emit('containerUpdate', {id: req.body.id, status: 'Stopped'});
				io.to(req.session.user._id).emit('containerUpdate', {id: req.body.id, status: 'Stopped'});
				res.status(200).send('ok');

			} else {
				res.status(400).send(error);
			}
		});
	});

	app.post('/checkServerName', function (req, res) {
		SC.findServerByName({ name: req.body.name, user: req.session.user.user }, function (e, o) {
			if (o === null) {
				res.status(200).send('ok');
			} else {
				res.status(400).send('server found');
			}
		});
	});

	app.post('/logout', function (req, res) {
		res.clearCookie('user');
		res.clearCookie('pass');
		req.session.destroy(function (e) {
			res.status(200).send('ok');
		});
	});

	// creating new accounts
	app.get('/signup', function (req, res) {
		if (req.cookies.user != undefined || req.cookies.pass != undefined) {
			AM.isAdmin(req.session.user._id, function (e, user) {
				if(user.admin) {
					res.render('signup', { title: 'Signup', adminLogged: user.admin });
				} else {
					res.render('signup', { title: 'Signup' });
				}
			});
		} else {
			res.render('signup', { title: 'Signup' });
		}

	});

	app.get('/account', function (req, res) {
		if (req.session.user == null) {
			// if user is not logged-in redirect back to login page
			res.redirect('/');
		} else {
			AM.isAdmin(req.session.user._id, function (e, user) {
				var id = req.session.user._id;
				if (user.admin && req.query.id != undefined) {
					id = req.query.id;
				}
				SC.getUserByUserId(id, function (e, o) {
					if (e) {
						// nothing
					} else {
						res.render('profile', { admin: user.admin, user: o, title: 'Account Settings' });
					}
				});
			});
		}
	});

	app.post('/signup', function (req, res) {
		AM.addNewAccount({
			name: req.body.name,
			email: req.body.email,
			user: req.body.user,
			pass: req.body.pass,
			admin: req.body.admin,
			servers: []
		}, function (e) {
			if (e) {
				res.status(400).send(e);
			} else {
				res.status(200).send('ok');
			}
		});
	});

	// password reset
	app.post('/lost-password', function (req, res) {
		// look up the user's account via their email
		AM.getAccountByEmail(req.body.email, function (o) {
			if (o) {
				EM.dispatchResetPasswordLink(o, function (e, m) {
					// this callback takes a moment to return
					// TODO add an ajax loader to give user feedback
					if (!e) {
						res.status(200).send('ok');
					} else {
						for (var k in e) {
							console.log('ERROR : ', k, e[k]);
						}
						res.status(400).send('unable to dispatch password reset');
					}
				});
			} else {
				res.status(400).send('email-not-found');
			}
		});
	});

	app.get('/reset-password', function (req, res) {
		var email = req.query.e;
		var passH = req.query.p;
		AM.validateResetLink(email, passH, function (e) {
			if (e != 'ok') {
				res.redirect('/');
			} else {
				// save the user's email in a session instead of sending to the client
				req.session.reset = { email: email, passHash: passH };
				res.render('reset', { title: 'Reset Password' });
			}
		});
	});

	app.post('/reset-password', function (req, res) {
		var nPass = req.body.pass;
		// retrieve the user's email from the session to lookup their account and reset password
		var email = req.session.reset.email;
		// destory the session immediately after retrieving the stored email
		req.session.destroy();
		AM.updatePassword(email, nPass, function (e, o) {
			if (o) {
				res.status(200).send('ok');
			} else {
				res.status(400).send('unable to update password');
			}
		});
	});

	// view & delete accounts
	app.post('/fetchServer', function (req, res) {
		SM.getServerById(req.body.id, function (e, obj) {
			if (!e) {
				var results = {
					name: obj.name,
					conf: obj.conf,
					tag: obj.tag,
					timezone: obj.timezone,
					ownerId: obj.userId
				};
				res.status(200).send(results);
			} else {
				res.status(400).send('server not found');
			}
		});
	});

	app.get('/print', function (req, res) {
		AM.getAllRecords(function (e, accounts) {
			res.render('print', { title: 'Account List', accts: accounts });
		});
	});

	app.post('/delete', function (req, res) {
		AM.isAdmin(req.session.user._id, function (e, user) {
			if (e) {
				res.status(400).send('user not found');
			} else {
				var id = req.session.user._id;
				if (user.admin && req.body.id != undefined) {
					id = req.body.id;
				}
				console.log('Attemtping to delete user:' + id);
				SC.deleteAccount(id, function (e, o) {
					if (e) {
						res.status(400).send('user not found');
					} else {
						if (!user.admin) {
							res.clearCookie('user');
							res.clearCookie('pass');
							req.session.destroy(function (e) {
								res.status(200).send('ok');
							});
						} else {
							res.status(200).send('ok');
						}
					}
				});
			}
		});

		// AM.deleteAccount(req.body.id, function(e, obj){
		// 	if (!e){
		// 		res.clearCookie('user');
		// 		res.clearCookie('pass');
		// 		req.session.destroy(function(e){ res.status(200).send('ok'); });
		// 	} else{
		// 		res.status(400).send('record not found');
		// 	}
		// });
	});

	app.get('/reset', function (req, res) {
		AM.delAllRecords(function () {
			SM.delAllRecords(function () {
				res.redirect('/print');
			});
		});
	});

	app.get('/resetServers', function (req, res) {
		if (req.session.user == null) {
			res.redirect('/');
		} else {
			AM.delAllServers(req.session.user._id, function () {
				SM.delAllRecords();
				res.redirect('/home');
			});
		}
	});

	// Stop docker
	app.post('/stopDocker', function (req, res) {
		DM.stopDocker(req.body.id, function (e, d) {
			if (!e) {
				// nothing
			} else {
				// nothing
			}
		});
	});

	// Docker Manager
	app.post('/dockerManager/add', function (req, res) {
		AM.isAdmin(req.session.user._id, function (e, user) {
			if (e) {
				res.status(400).send(e);
			} else {
				if (!user.admin) {
					res.status(400).send('account-not-admin');
				} else {
					DM.addDockerManager({url: req.body.url}, function (e, o) {
						if (e) {
							res.status(400).send(e);
						}	else {
							DM.getDockerManager({url: req.body.url}, function(e, o) {
								if(e) {
									res.status(400).send(e);
								} else {
									var row = dockerRow({docker: o});
									var io = app.get('socketio');
									io.to('admin').emit('addDockerManager', {row: row});
									res.status(200).send('ok');
								}
							});
						}
					});
				}
			}
		});
	});

	app.post('/dockerManager/delete', function(req, res) {
		async.waterfall([
			function(callback) {
				AM.isAdmin(req.session.user._id, callback);
			},
			function(user, callback) {
				if (user.admin) {
					DM.deleteDockerManager(req.body.id, callback);
				} else {
					callback('account-not-admin');
				}
			}
		], function(error) {
			if (error) {
				res.status(400).send(error);
			} else {
				var io = app.get('socketio');
				io.to('admin').emit('removeDockerManager', {id: req.body.id});
				res.status(200).send('ok');
			}
		});
	});

	// Everything else
	app.get('*', function (req, res) {
		res.render('404', { title: 'Page Not Found'});
	});

	// Create Docker
	app.get('/');
};
