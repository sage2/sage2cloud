#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}"  )" && pwd  )"

cd $DIR

WIN_DIR="SAGE2_DisplayClientWin"
PUBLIC_DIR="../../public/downloads/$1/$2/"

mkdir -p "$WIN_DIR"
unzip -d "$WIN_DIR" SAGE2_client-win32-x64.zip
mkdir -p "$PUBLIC_DIR"

DISPLAY=0
while [  $DISPLAY -lt $3 ]; do
	ZIPFILE="SAGE2_DisplayClientWin-$1-$2-$DISPLAY.zip"
	rm -rf $WIN_DIR/SAGE2_Display.bat
	printf "SAGE2_client-win32-x64\\Fullscreen.bat -s $4 -d $DISPLAY\n" > $WIN_DIR/SAGE2_Display.bat
	chmod 755 "$WIN_DIR/SAGE2_Display.bat"
	
	zip -r "$ZIPFILE" "$WIN_DIR/"
	mv "$ZIPFILE" "$PUBLIC_DIR"
	let DISPLAY=DISPLAY+1 
done

rm -rf "$WIN_DIR"



