#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}"  )" && pwd  )"

cd $DIR

MAC_DIR="SAGE2_DisplayClientMac"
PUBLIC_DIR="../../public/downloads/$1/$2/"

mkdir -p "$MAC_DIR"
unzip -d "$MAC_DIR" SAGE2_client-darwin-x64.zip
mkdir -p "$PUBLIC_DIR"

DISPLAY=0
while [  $DISPLAY -lt $3 ]; do
	ZIPFILE="SAGE2_DisplayClientMac-$1-$2-$DISPLAY.zip"
	rm -rf $MAC_DIR/SAGE2_Display.sh
	printf "#!/bin/bash\n\n./SAGE2_client-darwin-x64/Fullscreen.command -s $4 -d $DISPLAY\n" > $MAC_DIR/SAGE2_Display.sh
	chmod 755 "$MAC_DIR/SAGE2_Display.sh"
	
	zip -r "$ZIPFILE" "$MAC_DIR/"
	mv "$ZIPFILE" "$PUBLIC_DIR"
	let DISPLAY=DISPLAY+1 
done

rm -rf "$MAC_DIR"



