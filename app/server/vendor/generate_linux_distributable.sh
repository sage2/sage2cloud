#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}"  )" && pwd  )"

cd $DIR

LINUX_DIR="SAGE2_DisplayClientLinux"
PUBLIC_DIR="../../public/downloads/$1/$2/"

mkdir -p "$LINUX_DIR"
unzip -d "$LINUX_DIR" SAGE2_client-linux-x64.zip
mkdir -p "$PUBLIC_DIR"

DISPLAY=0
while [  $DISPLAY -lt $3 ]; do
	ZIPFILE="SAGE2_DisplayClientLinux-$1-$2-$DISPLAY.zip"
	rm -rf $LINUX_DIR/SAGE2_Display.sh
	printf "#!/bin/bash\n\n./SAGE2_client-linux-x64/Fullscreen.command -s $4 -d $DISPLAY\n" > $LINUX_DIR/SAGE2_Display.sh
	chmod 755 "$LINUX_DIR/SAGE2_Display.sh"
	
	zip -r "$ZIPFILE" "$LINUX_DIR/"
	mv "$ZIPFILE" "$PUBLIC_DIR"
	let DISPLAY=DISPLAY+1 
done

rm -rf "$LINUX_DIR"



