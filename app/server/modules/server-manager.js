// SAGE2 is available for use under the SAGE2 Software License
//
// University of Illinois at Chicago's Electronic Visualization Laboratory (EVL)
// and University of Hawai'i at Manoa's Laboratory for Advanced Visualization
// and Applications (LAVA)
//
// See full text, terms and conditions in the LICENSE.txt included file
//
// Copyright (c) 2017

var mongoose	= require('mongoose'),
	Server		= require('../models/server.js');

exports.addNewServer = function (newData, callback) {
	if (newData.conf.resolution.width < 0) {
		callback('incorrect-width');
		return;
	}
	if (newData.conf.resolution.height < 0) {
		callback('incorrect-height');
		return;
	}
	if (newData.conf.layout.columns < 0) {
		callback('incorrect-columns');
		return;
	}
	if (newData.conf.layout.rows < 0) {
		callback('incorrect-rows');
		return;
	}

	newData.url = newData.url + ':' + newData.conf.port;
	Server.findOne({name: newData.name, user: newData.user}, function (error, server) {
		if (error) {
			callback(error);
		}			else if (server) {
			callback('servername-taken');
		}			else {
			var newServer = new Server(newData);
			newServer.save(function (error) {
				callback(error, newServer);
			});
		}
	});
};

exports.updateServer = function (server, callback) {
	Server.findOne({_id: server.id}, function (e, s) {
		if (e) {
			callback(e);
		} else {
			s.name = server.name;
			s.conf = server.conf;
			s.tag = server.tag;
			s.timezone = server.timezone;
			//s.user = server.user;
			//s.userId = server.userId;
			s.save(callback);
		}
	});
};
exports.deleteServer = function (id, callback) {
	Server.findOne({_id: id}).remove(callback);
};

exports.getServerById = function (id, callback) {
	Server.findOne({_id: id}, callback);
};

exports.getServerByUserAndName = function (o, callback) {
	Server.findOne({user: o.user, name: o.name}, callback);
};

exports.getServersByUserId = function (id, callback) {
	Server.find({ userId: id }, callback);
};

exports.getAllServers = function (callback) {
	Server.find({}, function (error, servers) {
		if (error) {
			callback(error);
		}		else if (servers === null) {
			callback(null, []);
		}		else {
			callback(null, servers);
		}
	});
};

exports.getContainerByServerId = function (id, callback) {
	Server.findOne({_id: id}, function (error, server) {
		if (error) {
			callback(error);
		}		else {
			callback(null, server.containers.sage2);
		}
	});
};

exports.dropServers = function (callback) {
	this.countServers(function (error, count) {
		if (error) {
			callback(error);
		}		else if (count === 0) {
			callback(null);
		}		else {
			var collection = mongoose.connection.collections[Server.collection.collectionName];
			collection.drop(function (err) {
				if (err) {
					if (err.message == 'ns not found') {
						callback(null);
					}
					callback(err);
				}				else {
					callback(null);
				}
			});
		}
	});
};

exports.countServers = function (callback) {
	Server.count({}, function (error, count) {
		if (error) {
			callback(error);
		}		else {
			callback(null, count);
		}
	});
};

exports.getNextPort = function (callback) {
	this.countServers(function (error, count) {
		// If no servers, start with port 1024
		if (!error && count === 0) {
			callback(null, 1025);
		} else {
			Server.findOne({}).sort('-conf.port').exec(function (error, server) {
				if (error) {
					callback(error);
				} else {
					callback(null, server.conf.port + 1);
				}
			});
		}
	});
};
