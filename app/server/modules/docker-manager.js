// SAGE2 is available for use under the SAGE2 Software License
//
// University of Illinois at Chicago's Electronic Visualization Laboratory (EVL)
// and University of Hawai'i at Manoa's Laboratory for Advanced Visualization
// and Applications (LAVA)
//
// See full text, terms and conditions in the LICENSE.txt included file
//
// Copyright (c) 2017

var async			= require('async'),
	config			= require('../../../config'),
	dHub			= require('docker-hub-api'),
	Docker			= require('dockerode'),
	exec			= require('child_process').exec,
	fs				= require('fs'),
	mongoose		= require('mongoose'),
	DockerManager	= require('../models/dockerManager.js'),
	Image			= require('../models/image.js');

var _this = this;

exports.createContainer = function(server, callback) {
	var docker = getDocker(server.docker);
	var sage2Data = 'sage2Data-' + server.user + '-' + server.name;
	var image = 'sage2/master:' + server.tag;

	var opts = {
		name: 'sage2-' + server.user + '-' + server.name,
		HostConfig: {
			PortBindings: {},
			VolumesFrom: [sage2Data]
		},
		Image: image,
		Cmd: [],
		ExposedPorts: {}
	};

	opts.HostConfig.PortBindings[server.conf.port + '/tcp'] = [{
		HostPort: String(server.conf.port),
		HostIp: '0.0.0.0 '
	}];
	opts.HostConfig.PortBindings[server.conf.index_port + '/tcp'] = [{
		HostPort: String(server.conf.index_port),
		HostIp: '0.0.0.0'
	}];
	opts.ExposedPorts[server.conf.port + '/tcp'] = { };
	opts.ExposedPorts[server.conf.index_port + '/tcp'] = { };

	async.series([
		function(callback) {
			docker.createContainer({
				name: sage2Data,
				Image: image,
				Volumes: {
					'/sage2/config': {},
					'/sage2/keys': {},
					'/root/Documents/SAGE2_Media': {}
				}
			}, callback);
		},
		function(callback) {
			docker.run(image,
				['bash', '-c', 'echo "' + JSON.stringify(server.conf).replace(/"/g, '\\"') + '" > /sage2/config/docker-cfg.json'],
				process.stdout, {
					VolumesFrom: [sage2Data],
					AutoRemove: true
				}, callback);
		},
		function(callback) {
			fs.readFile('certs/' + config.certs.ca, function(error, data) {
				docker.run(image,
					['bash', '-c', 'echo "' + data.toString() + '" > /sage2/keys/' + config.certs.ca],
					process.stdout, {
						VolumesFrom: [sage2Data],
						AutoRemove: true
					}, callback);
			});
		},
		function(callback) {
			fs.readFile('certs/' + config.certs.cert, function(error, data) {
				docker.run(image,
					['bash', '-c', 'echo "' + data.toString() + '" > /sage2/keys/' + config.certs.cert],
					process.stdout, {
						VolumesFrom: [sage2Data],
						AutoRemove: true
					}, callback);
			});
		},
		function(callback) {
			fs.readFile('certs/' + config.certs.key, function(error, data) {
				docker.run(image,
					['bash', '-c', 'echo "' + data.toString() + '" > /sage2/keys/' + config.certs.key],
					process.stdout, {
						VolumesFrom: [sage2Data],
						AutoRemove: true
					}, callback);
			});
		},
		function(callback) {
			docker.createContainer(opts, callback);
		}
	], function(error, results) {
		if (error) {
			callback(error);
		} else {
			callback(null, {
				sage2: results[5].id,
				data: results[0].id
			});
		}
	});
};

exports.updateContainer = function(server, callback) {
	var docker = getDocker(server.docker);

	var image = 'sage2/master:';

	if(server.tag === undefined || server.tag === null) {
		image = image + 'latest';
	} else {
		image = image + server.tag;
	}

	var sage2Data = 'sage2Data-' + server.user + '-' + server.name;
	var image = 'sage2/master:' + server.tag;

	var opts = {
		name: 'sage2-' + server.user + '-' + server.name,
		HostConfig: {
			PortBindings: {},
			VolumesFrom: [sage2Data]
		},
		Image: image,
		Cmd: [],
		ExposedPorts: {}
	};

	opts.HostConfig.PortBindings[server.conf.port + '/tcp'] = [{
		HostPort: String(server.conf.port),
		HostIp: '0.0.0.0 '
	}];
	opts.HostConfig.PortBindings[server.conf.index_port + '/tcp'] = [{
		HostPort: String(server.conf.index_port),
		HostIp: '0.0.0.0'
	}];
	opts.ExposedPorts[server.conf.port + '/tcp'] = { };
	opts.ExposedPorts[server.conf.index_port + '/tcp'] = { };

	var container = docker.getContainer(server.containers.sage2);
	container.inspect(function(error, status) {
		if(error) {
			callback(error);
		} else {
			async.series([
				function(callback) {
					if(status.State.Running) {
						container.stop(callback);
					}
					callback(null);
				},
				function(callback) {
					container.remove(callback);
				},
				function(callback) {
					docker.createContainer(opts, callback);
				},
				function(callback) {
					Image.findOne({ tag: server.tag }, callback);
				}
			], function(error, results) {
				if (error) {
					callback(error);
				} else {
					server.containers.sage2 = results[2].id;
					server.digest = results[3].digest;
					server.save(callback);
				}
			});
		}
	});
};

exports.updateContainerConf = function(server, callback) {
	var docker = getDocker(server.docker);
	var sage2Data = 'sage2Data-' + server.user + '-' + server.name;

	var image = 'sage2/master:' + server.tag;

	docker.run(image,
		['bash', '-c', 'echo "' + JSON.stringify(server.conf).replace(/"/g, '\\"') + '" > /sage2/config/docker-cfg.json'],
		process.stdout, {
			VolumesFrom: [sage2Data],
			AutoRemove: true
		}, callback);
};

exports.startContainer = function (server, callback) {
	var docker = getDocker(server.docker);
	var container = docker.getContainer(server.containers.sage2);
	var timezone = server.timezone === undefined ? 'America/Chicago' : server.timezone;

	async.waterfall([
		function(callback) {
			container.start(callback);
		},
		function(emptyArg, callback) {
			container.exec({Env: ['CONTAINER_TIMEZONE=' + timezone], Cmd: ['/sage2/bin/docker_set_timezone.sh'], AttachStdout: true, AttachSterr: true, HostConfig: {Privileged: true}}, callback);
		},
		function(exec, callback) {
			exec.start({'Detach': false, 'Tty': false, stream: true, stdin: false, stdout: true, stderr: true}, function(errror, stream) {

				docker.modem.demuxStream(stream, process.stdout, process.stderr);

				exec.inspect(function(err, data) {
					if (err) return;
					callback(null, null);
				});
			});
		}
	], callback);
};

exports.stopContainer = function (server, callback) {
	var docker = getDocker(server.docker);
	var container = docker.getContainer(server.containers.sage2);

	container.stop(callback);
};

exports.reloadConf = function (server, callback) {
	var cmd = './reloadConfiguration.sh ' + server.user + ' ' + server.id;
	exec(cmd, function (error, stdout, stderr) {
		if (error) {
			callback(error);
		} else {
			callback(null);
		}
	});
};

exports.restartContainer = function (server, callback) {
	_this.stopContainer(server.container, function (err) {
		_this.startContainer(server, function (err) {
			if (err) {
				callback(err);
			} else {
				callback(null);
			}
		});
	});
};

exports.deleteContainer = function (server, callback) {
	var docker = getDocker(server.docker);

	if (server.shouldDeleteData === undefined || server.shouldDeleteData === null) {
		server.shouldDeleteData = true;
	}

	if (server.shouldDeleteData) {
		async.series([
			function(callback) {
				docker.getContainer(server.containers.sage2).remove(callback);
			},
			function(callback) {
				docker.getContainer(server.containers.data).remove(callback);
			}
		], function(error, results) {
			callback(error);
		});
	} else {
		docker.getContainer(server.containers.sage2).remove(callback);
	}
};

exports.getStatus = function (server, callback) {
	var docker = getDocker(server.docker);
	var container = docker.getContainer(server.container);

	if (container.id !== '') {
		container.inspect(function (error, data) {
			if (error) {
				callback(error);
			} else {
				callback(null, data);
			}
		});
	} else {
		callback('container-id-empty');
	}
};

exports.addDockerManager = function (docker, callback) {
	DockerManager.findOne({ url: docker.url }, function (e, o) {
		if (e) {
			callback(e);
		}	else if (o) {
			callback('dockerManager-exists');
		}	else {
			var newDM = new DockerManager(docker);
			newDM.save(function (error) {
				callback(error, newDM);
			});
		}
	});
};

exports.deleteDockerManager = function(id, callback) {
	DockerManager.findOne({ _id: id }).remove(callback);
};

exports.getDockerManagers = function(callback) {
	DockerManager.find({}, function(error, dockers) {
		if (error) {
			callback(error);
		} else {
			async.each(dockers, function(docker, callback) {
				var d = new Docker({
					protocol: 'https',
					host: docker.url,
					port: 2376,
					ca: fs.readFileSync('certs/' + config.certs.ca),
					cert: fs.readFileSync('certs/' + config.certs.cert),
					key: fs.readFileSync('certs/' + config.certs.key),
					timeout: 500,
					version: 'v1.24'
				});

				d.ping(function(error, data) {
					if (error) {
						docker.status = 'Offline';
					} else {
						docker.status = 'Online';
					}
					callback();
				});
			}, function(error) {
				if (error) {
					callback(error);
				} else {
					callback(null, dockers);
				}
			});
		}
	});
};

exports.getDockerManager = function(o, callback) {
	DockerManager.findOne(o, function(error, docker) {
		if (error) {
			callback(error);
		} else {
			var d = new Docker({
				protocol: 'https',
				host: docker.url,
				port: 2376,
				ca: fs.readFileSync('certs/' + config.certs.ca),
				cert: fs.readFileSync('certs/' + config.certs.cert),
				key: fs.readFileSync('certs/' + config.certs.key),
				timeout: 500,
				version: 'v1.24'
			});

			d.ping(function(error, data) {
				if (error) {
					console.log(error);
					docker.status = 'Offline';
				} else {
					docker.status = 'Online';
				}
				callback(null, docker);
			});
		}
	});
};


exports.dropDockerManager = function (callback) {
	DockerManager.count({}, function (error, count) {
		if (error) {
			callback(error);
		} else if (count === 0) {
			callback(null);
		} else {
			var collection = mongoose.connection.collections[DockerManager.collection.collectionName];
			collection.drop(callback);
		}
	});
};

exports.countDockerManager = function (callback) {
	DockerManager.count({}, callback);
};

exports.getDockerImages = function(callback) {
	Image.find({}, null, {sort: {last_updated: -1}}, callback);
}

exports.getDigest = function(tag, callback) {
	Image.findOne({ tag: tag }, callback);
}

function getDocker(url) {
	if (url === undefined || url === null || url === config.hostname) {
		return new Docker();
	} else {
		return new Docker({
			protocol: 'https',
			host: url,
			port: 2376,
			ca: fs.readFileSync('certs/' + config.certs.ca),
			cert: fs.readFileSync('certs/' + config.certs.cert),
			key: fs.readFileSync('certs/' + config.certs.key),
			timeout: 500,
			version: 'v1.24'
		});
	}
}

function checkForUpdates() {
	console.log("Checking for updates");
	dHub.tags('sage2', 'master').then(function(results) {
		if(results.results != null) {
			results = results.results;
		}
		DockerManager.find({}, function(error, dockers) {
			if(error) {
				console.log("Error: " + error);
			} else {

				updateImages(new Docker(), results, function(error) {
					console.log("Error: " + error);
				});

				async.each(dockers, function(docker, callback) {
					var d = new Docker({
						protocol: 'https',
						host: docker.url,
						port: 2376,
						ca: fs.readFileSync('certs/' + config.certs.ca),
						cert: fs.readFileSync('certs/' + config.certs.cert),
						key: fs.readFileSync('certs/' + config.certs.key),
						timeout: 500,
						version: 'v1.24'
					});
					updateImages(d, results, function(error) {
						console.log("Error: " + error);
					});
				}, function(error) {
					if(error) {
						console.log("Error: " + error);
					}
				});
			}
		});
	});
}

function updateImages(docker, tags, callback) {
	// Add latest tag
	async.each(tags, function(image, callback) {
		var tag = image.name;
		var last_updated = image.last_updated;
		Image.findOne({ tag: tag }, function(error, image) {
			if(error) {
				callback(error);
			} else {
				docker.pull('sage2/master:' + tag, function(error, stream) {
					if(error) {
						callback(error);
					} else {
						docker.modem.followProgress(stream, onFinished);

						function onFinished(error, output) {
							if(error) {
								callback(error);
							} else {
								for(var i=0; i<output.length; i++) {
									if(output[i].status.includes("Digest")) {
										var digest = output[i].status.substring(output[i].status.indexOf("sha256"));
										if(!image) {
											// New image, not yet in database
											image = new Image();
											image.tag = tag;
										}
										image.digest = digest;
										image.last_updated = last_updated;
										image.save(callback);
									}
								}
							}
						}
					}
				});
			}
		});
	}, callback);
}

// Check every hour
setInterval(checkForUpdates, 3600000);

// Do it once when we start
checkForUpdates();
