// SAGE2 is available for use under the SAGE2 Software License
//
// University of Illinois at Chicago's Electronic Visualization Laboratory (EVL)
// and University of Hawai'i at Manoa's Laboratory for Advanced Visualization
// and Applications (LAVA)
//
// See full text, terms and conditions in the LICENSE.txt included file
//
// Copyright (c) 2017

var Account		= require('../models/account.js'),
	crypto		= require('crypto'),
	mongoose	= require('mongoose'),
	moment		= require('moment');

exports.autoLogin = function (user, pass, callback) {
	Account.findOne({user: user}, function (e, o) {
		if (o) {
			o.pass == pass ? callback(o) : callback(null);
		} else {
			callback(null);
		}
	});
};

exports.manualLogin = function (user, pass, callback) {
	Account.findOne({user: user}, function (e, o) {
		if (o == null) {
			callback('user-not-found');
		} else {
			validatePassword(pass, o.pass, function (err, res) {
				if (res) {
					callback(null, o);
				} else {
					callback('invalid-password');
				}
			});
		}
	});
};

// record insertion, update & deletion methods

exports.addNewAccount = function (newData, callback) {
	var _this = this;
	Account.findOne({user: newData.user}, function (e, o) {
		if (o) {
			callback('username-taken');
		} else {
			Account.findOne({email: newData.email}, function (e, o) {
				if (o) {
					callback('email-taken');
				} else {
					// check if this is admin
					_this.countAccounts(function (error, count) {
						if (error) {
							callback(error);
						} else {
							if (count === 0) {
								// this is admin
								newData.admin = 'on';
							}

							if (newData.admin != undefined) {
								if (newData.admin === 'on') {
									newData.admin = true;
								} else {
									newData.admin = false;
								}
							}
							saltAndHash(newData.pass, function (hash) {
								newData.pass = hash;
								// append date stamp when record was created //
								newData.date = moment().format('MMMM Do YYYY, h:mm:ss a');
								var newAccount = new Account(newData);
								newAccount.save(function (error) {
									callback(error, newAccount);
								});
							});
						}
					});
				}
			});
		}
	});
};

exports.getAllAccounts = function (callback) {
	Account.find({}, function (error, accounts) {
		if (error) {
			callback(error);
		} else if (accounts === null) {
			callback([]);
		} else {
			callback(null, accounts);
		}
	});
};

exports.updateAccount = function (newData, callback) {
	Account.findOne({_id: newData.id}, function (e, o) {
		o.name 		= newData.name;
		o.email 	= newData.email;
		if (newData.admin != undefined) {
			if (newData.admin === 'on') {
				o.admin = true;
			} else {
				o.admin = false;
			}
		}
		if (newData.pass == '') {
			o.save(function (e) {
				if (e) {
					callback(e);
				} else {
					callback(null, o);
				}
			});
		} else {
			saltAndHash(newData.pass, function (hash) {
				o.pass = hash;
				o.save(function (e) {
					if (e) {
						callback(e);
					} else {
						callback(null, o);
					}
				});
			});
		}
	});
};

exports.updatePassword = function (email, newPass, callback) {
	Account.findOne({email: email}, function (e, o) {
		if (e) {
			callback(e, null);
		} else {
			saltAndHash(newPass, function (hash) {
				o.pass = hash;
				o.save(callback);
			});
		}
	});
};

// account lookup methods

exports.deleteAccount = function (id, callback) {
	Account.findOne({_id: id}).remove(function (error) {
		if (error) {
			callback(error);
		}		else {
			callback(null);
		}
	});
};

exports.getAccountByEmail = function (email, callback) {
	Account.findOne({email: email}, function (e, o) {
		callback(o);
	});
};

exports.getAccountByUser = function (user, callback) {
	Account.findOne({user: user}, function (e, o) {
		callback(o);
	});
};

exports.getAccountByUserId = function (id, callback) {
	Account.findOne({_id: id}, callback);
};

exports.validateResetLink = function (email, passHash, callback) {

	/*
	accounts.find({ $and: [{email: email, pass: passHash}] }, function(e, o) {
		callback(o ? 'ok' : null);
	}); */
};

exports.delAllServers = function (id, callback) {

	/*
	accounts.findOne({_id: getObjectId(id)}, function(e, o) {
		if (o == null) {
			callback('user-not-found');
		} else {
			o.servers = [];
			accounts.save(o, {safe: true}, function(e) {
				if (e) {
					callback(e);
				}				else {
					callback(null, o);
				}
			});
		}
	});
	*/
};

exports.delServerFromAccount = function (data, callback) {

	/*
	accounts.findOne({_id: getObjectId(data.accountId)}, function(e, o) {
		if (o == null) {
			callback('user-not-found');
		} else {
			var index = -1;
			for (var i = 0; i < o.servers.length; i++) {
				if (o.servers[i] == data.serverId) {
					index = i;
				}
			}
			if (index > -1) {
				o.servers.splice(index, 1);
			}
			accounts.sAdmin;
			save(o, {safe: true}, function(e) {
				if (e) {
					callback(e);
				}				else {
					callback(null, o);
				}
			});
		}
	});
	*/

};

exports.dropAccounts = function (callback) {
	this.countAccounts(function (error, count) {
		if (error) {
			callback(error);
		}		else if (count === 0) {
			callback(null);
		}		else {
			var collection = mongoose.connection.collections[Account.collection.collectionName];
			// if(collection === )
			// console.log(collection);
			collection.drop(function (err) {
				if (err) {
					callback(err);
				}				else {
					callback(null);
				}
			});
		}
	});
};

exports.countAccounts = function (callback) {
	Account.count({}, function (error, count) {
		if (error) {
			callback(error);
		}		else {
			callback(null, count);
		}
	});
};

exports.isAdmin = function (id, callback) {
	Account.findOne({_id: id}, callback);
};

// private encryption & validation methods

var generateSalt = function () {
	var set = '0123456789abcdefghijklmnopqurstuvwxyzABCDEFGHIJKLMNOPQURSTUVWXYZ';
	var salt = '';
	for (var i = 0; i < 10; i++) {
		var p = Math.floor(Math.random() * set.length);
		salt += set[p];
	}
	return salt;
};

var md5 = function (str) {
	return crypto.createHash('md5').update(str).digest('hex');
};

var saltAndHash = function (pass, callback) {
	var salt = generateSalt();
	callback(salt + md5(pass + salt));
};

var validatePassword = function (plainPass, hashedPass, callback) {
	var salt = hashedPass.substr(0, 10);
	var validHash = salt + md5(plainPass + salt);
	callback(null, hashedPass === validHash);
};
