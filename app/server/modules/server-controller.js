// SAGE2 is available for use under the SAGE2 Software License
//
// University of Illinois at Chicago's Electronic Visualization Laboratory (EVL)
// and University of Hawai'i at Manoa's Laboratory for Advanced Visualization
// and Applications (LAVA)
//
// See full text, terms and conditions in the LICENSE.txt included file
//
// Copyright (c) 2017

var async = require('async');
var shell = require('shelljs');

var AM = require('./account-manager');
var SM = require('./server-manager');
var DM = require('./docker-manager');

exports.addServer = function (server, callback) {
	server.url = 'https://' + server.docker;
	server.conf.host = server.docker;
	server.conf.displays = [];
	for (var i = 0; i < server.conf.rows; i++) {
		for (var j = 0; j < server.conf.columns; j++) {
			server.conf.displays.push({ row: i, column: j });
		}
	}

	if (server.conf.remote_sites === undefined) {
		server.conf.remote_sites = [];
	}

	async.waterfall([
		function(callback) {
			SM.getNextPort(callback);
		},
		function(index_port, callback) {
			server.conf.index_port = index_port;
			server.conf.port = index_port + 1;
			DM.createContainer(server, callback);
		},
		function(containers, callback) {
			server.containers = containers;
			DM.getDigest(server.tag, callback);
		},
		function(image, callback){
			server.digest = image.digest;
			SM.addNewServer(server, callback);
		}
		/*,
		function(s, callback) {
			var nmbDisplays = server.conf.layout.rows * server.conf.layout.columns;
			shell.exec('./app/server/vendor/generate_mac_distributable.sh ' + server.user + ' ' + server.name + ' ' + nmbDisplays + ' http://' + server.url, {silent:true}, function(code, stdout, stderr) {
				callback(null, null);
			});
		},
		function(s, callback) {
			var nmbDisplays = server.conf.layout.rows * server.conf.layout.columns;
			shell.exec('./app/server/vendor/generate_linux_distributable.sh ' + server.user + ' ' + server.name + ' ' + nmbDisplays + ' ' + server.url, {silent:true}, function(code, stdout, stderr) {
				callback(null, null);
			});
		},
		function(s, callback) {
			var nmbDisplays = server.conf.layout.rows * server.conf.layout.columns;
			shell.exec('./app/server/vendor/generate_windows_distributable.sh ' + server.user + ' ' + server.name + ' ' + nmbDisplays + ' ' + server.url, {silent:true}, function(code, stdout, stderr) {
				callback(null, null);
			});
		},*/


	], callback);
};

exports.startServer = function (id, callback) {
	SM.getServerById(id, function (error, server) {
		DM.startContainer(server, callback);
	});
};

exports.findServerByName = function (server, callback) {
	SM.getServerByUserAndName(server, function (error, server) {
		if (error) {
			callback(error);
		} else {
			callback(null, server);
		}
	});
};

exports.stopServer = function (id, callback) {
	SM.getServerById(id, function (error, server) {
		DM.stopContainer(server, callback);
	});
};

exports.updateContainer = function(id, callback) {
	SM.getServerById(id, function (error, server) {
		DM.updateContainer(server, callback);
	});
};

exports.getUserByUserId = function (id, callback) {
	AM.getAccountByUserId(id, callback);
};

exports.getServersByUserId = function (id, callback) {
	SM.getServersByUserId(id, function (error, servers) {
		async.each(servers, getDockerStatus, function (error) {
			if (error) {
				callback(error);
			} else {
				callback(null, servers);
			}
		});
	});
};

exports.checkForUpdatesAllServers = function(callback) {
	SM.getAllServers(function (error, servers) {
		DM.getDockerImages(function(error, images) {

			var results = [];
			for(var i=0; i<servers.length; i++) {
				results[servers[i]._id] = "no-update";
				for(var j=0; j<images.length; j++) {
					if(servers[i].tag === undefined || servers[i].tag === null) {
						results[servers[i]._id] = "update";
					}
					else if(servers[i].tag === images[j].tag) {
						if(servers[i].digest === undefined || servers[i].digest === null) {
							results[servers[i]._id] = "update";
						}
						else if(images[j].digest !== servers[i].digest) {
							results[servers[i]._id] = "update";
						}
					}
				}
			}
			callback(null, results);
		});
	});


}

exports.checkForUpdates = function(id, callback) {
	SM.getServersByUserId(id, function (error, servers) {
		DM.getDockerImages(function(error, images) {

			var results = [];
			for(var i=0; i<servers.length; i++) {
				results[servers[i]._id] = "no-update";
				for(var j=0; j<images.length; j++) {
					if(servers[i].tag === undefined || servers[i].tag === null) {
						results[servers[i]._id] = "update";
					}
					else if(servers[i].tag === images[j].tag) {
						if(servers[i].digest === undefined || servers[i].digest === null) {
							results[servers[i]._id] = "update";
						}
						else if(images[j].digest !== servers[i].digest) {
							results[servers[i]._id] = "update";
						}
					}
				}
			}
			callback(null, results);
		});
	});

}

exports.getContainerStatus = function(id, callback) {
	SM.getServerById(id, function (error, server) {
		getDockerStatus(server, callback);
	});
};

exports.getAllServersUsersDockers = function (callback) {
	async.parallel([
		function (callback) {
			AM.getAllAccounts(callback);
		},
		function (callback) {
			SM.getAllServers(function(error, servers) {
				async.each(servers, getDockerStatus, function (error) {
					if (error) {
						callback(error);
					} else {
						callback(null, servers);
					}
				});

			});
		},
		function (callback) {
			DM.getDockerManagers(callback);
		}
	],
	function (err, results) {

		var data = {
			accounts: results[0],
			servers: results[1],
			dockerManagers: results[2]
		};
		callback(err, data);
	});
};

exports.deleteServer = function (id, callback) {
	async.waterfall([
		function(callback) {
			SM.getServerById(id, callback);
		},
		function(server, callback) {
			server.shouldDeleteData = true;
			DM.deleteContainer(server, function(error) {
				callback(error, server);
			});
		},
		function(server, callback) {
			SM.deleteServer(id, function(error) {
				callback(error, server);
			});
		}
	], function(error, results) {
		callback(error);
	});
};

exports.updateServer = function (server, callback) {
	var shouldStart = false;
	server.conf.displays = [];
	for (var i = 0; i < server.conf.rows; i++) {
		for (var j = 0; j < server.conf.columns; j++) {
			server.conf.displays.push({ row: i, column: j });
		}
	}

	if (server.conf.remote_sites === undefined) {
		server.conf.remote_sites = [];
	}

	async.waterfall([
		function(callback) {
			SM.getServerById(server.id, callback);
		},
		function(container, callback) {
			getDockerStatus(container, callback);
		},
		function(container, callback) {
			if (container.state === 'Running') {
				shouldStart = true;
				DM.stopContainer(container, function(error) {
					callback(error, container);
				});
			} else {
				callback(null, container);
			}
		},
		function(dbServer, callback) {
			// Save the ports and container information!
			server.url = dbServer.url;
			server.conf.host = dbServer.conf.host;
			server.conf.port = dbServer.conf.port;
			server.conf.index_port = dbServer.conf.index_port;
			server.containers = dbServer.containers;
			server.docker = dbServer.docker;
			DM.updateContainerConf(server, function(error, data) {
				callback(error, dbServer);
			});
		},
		function(dbServer, callback) {
			if(server.tag !== dbServer.tag) {
				dbServer.tag = server.tag;
				DM.updateContainer(dbServer, function(error, data) {
					callback(error, null);
				});
			} else {
				callback(null, null);
			}
		},
		function(emptyArg, callback) {
			SM.updateServer(server, function(error) {
				callback(error);
			});
		},
		function(callback) {
			if (shouldStart) {
				DM.startContainer(server, function(error, data) {
					callback(error);
				});
			} else {
				callback(null);
			}
		}
	], function(error, results) {
		callback(error);
	});
};

exports.deleteAllServersByAccount = function (id, callback) {
	var _this = this;
	this.getServersByUserId(id, function (error, servers) {
		async.each(servers, function (server, callback) {
			_this.deleteServer(server._id, callback);
		}, function (error) {
			if (error) {
				callback(error);
			} else {
				callback(null);
			}
		});
	});
};

// Account functions
exports.deleteAccount = function (id, callback) {
	// Delete All Servers
	this.deleteAllServersByAccount(id, function (error) {
		// Delete Account
		AM.deleteAccount(id, callback);
	});
};

var getDockerStatus = function (server, callback) {
	if (server === null) {
		server = {  };
		server.state = 'Offline';
		callback(null, server);
	} else {
		DM.getStatus({container: server.containers.sage2, docker: server.docker}, function (error, status) {
			if (error || status === null) {
				server.state = 'Offline';
			} else if (status.State.Running) {
				server.state = 'Running';
			} else if (status.State.Paused) {
				server.state = 'Paused';
			} else if (status.State.Restarting) {
				server.state = 'Restarting';
			} else if (status.State.Dead) {
				server.state = 'Dead';
			} else {
				server.state = 'Stopped';
			}
			callback(null, server);
		});
	}
};
