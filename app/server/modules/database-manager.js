// SAGE2 is available for use under the SAGE2 Software License
//
// University of Illinois at Chicago's Electronic Visualization Laboratory (EVL)
// and University of Hawai'i at Manoa's Laboratory for Advanced Visualization
// and Applications (LAVA)
//
// See full text, terms and conditions in the LICENSE.txt included file
//
// Copyright (c) 2017

var mongoose = require('mongoose');

module.exports = function (host, port, name, user, pass) {
	var dbURL;
	if (user === undefined || user === null) {
		dbURL = 'mongodb://' + host + ':' + port + '/' + name;
	} else {
		dbURL = 'mongodb://' + user + ':' + pass + '@' + host + ':' + port + '/' + name;
	}
	mongoose.connect(dbURL);

	this.close = function () {
		mongoose.connection.close();
	};

	return this;
};
