var mongoose = require('mongoose');

var accountSchema = mongoose.Schema({
	name: String,
	email: String,
	user: String,
	pass: String,
	date: String,
	admin: {
		type: Boolean,
		default: false
	}
});

module.exports = mongoose.model('Account', accountSchema);



