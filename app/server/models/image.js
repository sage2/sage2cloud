var mongoose = require('mongoose');

var imageSchema = mongoose.Schema({
	tag: String,
	digest: String,
	last_updated: Date
});

module.exports = mongoose.model('Image', imageSchema);


