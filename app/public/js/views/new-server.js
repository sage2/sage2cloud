// SAGE2 is available for use under the SAGE2 Software License
//
// University of Illinois at Chicago's Electronic Visualization Laboratory (EVL)
// and University of Hawai'i at Manoa's Laboratory for Advanced Visualization
// and Applications (LAVA)
//
// See full text, terms and conditions in the LICENSE.txt included file
//
// Copyright (c) 2017

$(document).ready(function () {
	new ServerController();
	var sv = new ServerValidator();

	var timeoutID = null;

	$('.name-error').hide();
	$('.error-width').hide();
	$('.error-height').hide();
	$('.error-rows').hide();
	$('.error-columns').hide();
	$('#new-server-submit').prop('disabled', false);
	$('#new-server-advanced').prop('disabled', true);

	function checkName (str) {
		var val = sv.validateName(str);
		if (val !== 'Success') {
			sv.showInvalidName(val);
		} else {
			$('.name-error').hide();
			$('#new-server-submit').prop('disabled', false);
		}
	}

	function checkNumber (target) {
		var val = sv.validateNumber(target);
		if (val !== 'Success') {
			sv.showInvalidField(val, target);
		} else {
			var field = '.' + target.id.replace(/server-/gi, 'error-');
			$(field).hide();
			$('#new-server-submit').prop('disabled', false);
		}
	}

	$('#server-name').keyup(function (e) {
		clearTimeout(timeoutID);
		timeoutID = setTimeout(checkName.bind(undefined, e.target.value), 500);
	});

	$('#server-width').keyup(function (e) {
		clearTimeout(timeoutID);
		timeoutID = setTimeout(checkNumber.bind(undefined, e.target), 500);
	});

	$('#server-height').keyup(function (e) {
		clearTimeout(timeoutID);
		timeoutID = setTimeout(checkNumber.bind(undefined, e.target), 500);
	});

	$('#server-rows').keyup(function (e) {
		clearTimeout(timeoutID);
		timeoutID = setTimeout(checkNumber.bind(undefined, e.target), 500);
	});

	$('#server-columns').keyup(function (e) {
		clearTimeout(timeoutID);
		timeoutID = setTimeout(checkNumber.bind(undefined, e.target), 500);
	});

	// handle new server click //
	$('#btn-new-server').click(function () {
		$('.modal-new-server').modal('show');
		$('#available-servers').removeAttr('disabled');
		$('#server-name').val('');
		$('.modal-new-server-title').text('New SAGE2 Server');
	});
});
