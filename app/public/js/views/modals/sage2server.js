// SAGE2 is available for use under the SAGE2 Software License
//
// University of Illinois at Chicago's Electronic Visualization Laboratory (EVL)
// and University of Hawai'i at Manoa's Laboratory for Advanced Visualization
// and Applications (LAVA)
//
// See full text, terms and conditions in the LICENSE.txt included file
//
// Copyright (c) 2017

/*global Sage2serverController*/
/*eslint no-undef: 1*/

$(document).ready(function () {

	new Sage2serverController();

	$('.modal-sage2server').on('shown.bs.modal', function() {
		if ($('.modal-sage2server').hasClass('configure-sage2server')) {
			$('.modal-sage2server-title').text('Configure SAGE2 Server');
			$('.btn-modal-sage2server-submit').text('Update');
			$('#available-servers-header').hide();
			$('#available-servers').hide();
		} else {
			$('.modal-sage2server-title').text('New SAGE2 Server');
			$('.btn-modal-sage2server-submit').text('Submit');
			$('#server-name').val('');
			$('.name-error').hide();
			$('#server-timezone').val(moment.tz.guess());
			$('#available-servers-header').show();
			$('#available-servers').show();
		}
	});
});

