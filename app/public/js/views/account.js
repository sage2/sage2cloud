// SAGE2 is available for use under the SAGE2 Software License
//
// University of Illinois at Chicago's Electronic Visualization Laboratory (EVL)
// and University of Hawai'i at Manoa's Laboratory for Advanced Visualization
// and Applications (LAVA)
//
// See full text, terms and conditions in the LICENSE.txt included file
//
// Copyright (c) 2017

$(document).ready(function () {
	var av = new AccountValidator();

	$('#account-form').ajaxForm({
		beforeSubmit: function (formData, jqForm, options) {
			console.log(formData);
			return av.validateForm();
		},
		success: function (responseText, status, xhr, $form) {
			if (status == 'success') {
				$('.modal-alert').modal('show');
			}
		},
		error: function (e) {
			if (e.responseText == 'email-taken') {
				av.showInvalidEmail();
			} else if (e.responseText == 'username-taken') {
				av.showInvalidUserName();
			}
		}
	});
	$('#name-tf').focus();

	$('.delete-account').click(function (e) {
		var _this = this;

		$.ajax({
			url: '/admin',
			type: 'POST',
			data: { action: 'deleteAccount', accountId: _this.id },
			success: function (data) {
				// window.location.href = '/';
			},
			error: function (jqXHR) {
				console.log(jqXHR.responseText + ' :: ' + jqXHR.statusText);
			}
		});
	});

	$('.configure-account').click(function (e) {
		window.location.href = '/account?id=' + this.id;
	});

	// customize the account settings form
	$('#user-tf').attr('disabled', 'disabled');
	$('#account-form-btn1').html('Delete');
	// $('#account-form-btn1').addClass('btn-danger');
	$('#account-form-btn2').html('Update');

	// setup the confirm window that displays when the user chooses to delete their account
	$('.modal-confirm').modal({ show: false, keyboard: true, backdrop: true });
	$('.modal-confirm .modal-header h4').text('Delete Account');
	$('.modal-confirm .modal-body p').html('Are you sure you want to delete the account?');
	$('.modal-confirm .cancel').html('Cancel');
	$('.modal-confirm .submit').html('Delete');
	$('.modal-confirm .submit').addClass('btn-danger');
	$('.modal-confirm .submit').addClass('delete-user-account');

	$('.modal-alert').modal({ show: false, keyboard: false, backdrop: 'static' });
	$('.modal-alert .modal-header h4').text('Account Updated!');
	$('.modal-alert .modal-body p').html('Account updated.');

	$('#account-form-btn1').click(function (e) {
		$('.modal-confirm').modal('show');
	});

	$('.delete-user-account').click(function (e) {
		$.ajax({
			url: '/delete',
			type: 'POST',
			data: {id: $('#userId').val()},
			success: function (data) {
				window.location.href = '/';
			},
			error: function (jqXHR) {
				console.log(jqXHR.responseText + ' :: ' + jqXHR.statusText);
			}
		});
	});
});
