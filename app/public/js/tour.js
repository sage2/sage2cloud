// Instance the tour
var tour = new Tour({
  steps: [
  {
    orphan: true,
    title: "SAGE2cloud Help",
    content: "Welcome to SAGE2cloud Tour. This tour will help you create your first SAGE2 server and explain to you the basics"
  },
  {
    element: ".btn-add-server",
    title: "Create a SAGE2 Server",
    content: "To create a new SAGE2 Server, you click here",
	backdrop: true,
	onNext: function(){
		// Do something
	}
  }
]});

$(document).ready(function () {

	// Initialize the tour
	tour.init();

	// Start the tour
	tour.start();
});
