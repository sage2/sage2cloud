// SAGE2 is available for use under the SAGE2 Software License
//
// University of Illinois at Chicago's Electronic Visualization Laboratory (EVL)
// and University of Hawai'i at Manoa's Laboratory for Advanced Visualization
// and Applications (LAVA)
//
// See full text, terms and conditions in the LICENSE.txt included file
//
// Copyright (c) 2017

function HomeController () {
	// bind event listeners to button clicks
	var _this = this;

	// handle user logout
	$('#btn-logout').click(function () {
		_this.attemptLogout();
	});

	// confirm account deletion
	$('#account-form-btn1').click(function () {
		$('.modal-confirm').modal('show');
	});

	// handle account deletion
	$('.modal-confirm .submit').click(function () {
		_this.deleteAccount();
	});

	$('.btn-add-server').click(function(e) {
		$('.modal-sage2server').removeClass('configure-sage2server');
		$('.modal-sage2server').addClass('add-sage2server');
		$('.modal-sage2server').modal('show');
	});

	// handle new server click
	// $('#btn-new-server').click(function(){
	// 	$('.modal-new-server').modal('show');
	// 	$('#server-name').val("");
	// });
	//

	this.deleteAccount = function ()	{
		$('.modal-confirm').modal('hide');
		var _this = this;
		$.ajax({
			url: '/delete',
			type: 'POST',
			data: { id: $('#userId').val()},
			success: function (data) {
				_this.showLockedAlert('Your account has been deleted.<br>Redirecting you back to the homepage.');
			},
			error: function (jqXHR) {
				console.log(jqXHR.responseText + ' :: ' + jqXHR.statusText);
			}
		});
	};

	this.attemptLogout = function ()	{
		var _this = this;
		$.ajax({
			url: '/logout',
			type: 'POST',
			data: {logout: true},
			success: function (data) {
				_this.showLockedAlert('You are now logged out.<br>Redirecting you back to the homepage.');
			},
			error: function (jqXHR) {
				console.log(jqXHR.responseText + ' :: ' + jqXHR.statusText);
			}
		});
	};

	this.showLockedAlert = function (msg) {
		$('.modal-alert').modal({ show: false, keyboard: false, backdrop: 'static' });
		$('.modal-alert .modal-header h4').text('Success!');
		$('.modal-alert .modal-body p').html(msg);
		$('.modal-alert').modal('show');
		$('.modal-alert button').click(function () {
			window.location.href = '/';
		});
		setTimeout(function () {
			window.location.href = '/';
		}, 3000);
	};
}

HomeController.prototype.onNewServerShowProgressBar = function () {
	$('.modal-alert').modal({ show: false, keyboard: false, backdrop: 'static' });
	$('.modal-alert .modal-header h4').text('Adding new SAGE2 Server');
	$('.modal-alert .modal-body p').html('<i class="fa fa-refresh fa-spin fa-2x fa-fw"></i>Please wait, creating server');
	$('.modal-alert').modal('show');
	$('#ok').prop('disabled', true);
	// $('.modal-alert button').off('click');
};

HomeController.prototype.onNewServerSuccess = function () {
	$('.modal-alert').modal('hide');

	// $('.modal-alert').modal({ show : false, keyboard : true, backdrop : true });
	// $('.modal-alert .modal-header h4').text('Success!');
	// $('.modal-alert .modal-body p').html('A new server has been added.');
	// $('.modal-alert').modal('show');
	// $('.modal-alert button').off('click');
};

HomeController.prototype.onUpdateSuccess = function () {
	$('.modal-alert').modal({ show: false, keyboard: true, backdrop: true });
	$('.modal-alert .modal-header h4').text('Success!');
	$('.modal-alert .modal-body p').html('Account updated.');
	$('.modal-alert').modal('show');
	$('.modal-alert button').off('click');
};
