// SAGE2 is available for use under the SAGE2 Software License
//
// University of Illinois at Chicago's Electronic Visualization Laboratory (EVL)
// and University of Hawai'i at Manoa's Laboratory for Advanced Visualization
// and Applications (LAVA)
//
// See full text, terms and conditions in the LICENSE.txt included file
//
// Copyright (c) 2017

function Sage2serverController () {

	$('.btn-modal-sage2server-submit').click(function (e) {
		e.stopImmediatePropagation();
		if ($(e.target).closest('.modal-sage2server').hasClass('configure-sage2server')) {
			var id = $('.modal-sage2server').attr('id');
			updateServer(id);
		} else if ($(e.target).closest('.modal-sage2server').hasClass('add-sage2server')) {
			$('.modal-sage2server').modal('hide');
			$('.modal-loading h4').text('Creating SAGE2 server ' + $('#server-name').val());
			$('.modal-loading p').text('Please wait');
			$('.modal-loading').modal('show');
			addServer();
		}
	});

	$(document).ready(function() {
		var isAdmin = $('.container-sage2list').hasClass('admin-page');
	});

	$('.modal-sage2server .btn-danger').click(function(e) {
		e.stopImmediatePropagation();
		$('.modal-sage2server').modal('hide');
	});

	var addServer = function() {
		$.ajax({
			url: '/home',
			type: 'POST',
			data: {
				docker: $('#available-servers option:selected').text(),
				tag: $('#available-server-versions option:selected').val(),
				name: $('#server-name').val(),
				conf: {
					resolution: {
						width: $('#server-width').val(),
						height: $('#server-height').val(),
					},
					layout: {
						rows: $('#server-rows').val(),
						columns: $('#server-columns').val(),
					},
					ui: {
						clock: 12,
						show_version: true,
						show_url: true,
					},
					background: {
						color: "#333333",
						watermark: {
							svg: "images/EVL-LAVA.svg",
							color: "rgba(255, 255, 255, 0.5)"
						}
					},
					alternate_hosts: [
						"127.0.0.1"
					],
					remote_sites: []
				},
				ownerId: $('#server-owner option:selected').val(),
				owner: $('#server-owner option:selected').text(),
				timezone: $('#server-timezone option:selected').text()
			},
			success: function (status) {
				$('.modal-loading').modal('hide');
			},
			error: function (jqXHR) {
				console.log(jqXHR.responseText + ' :: ' + jqXHR.statusText);
				$('.modal-loading').modal('hide');
			}
		});

	};

	var updateServer = function(id) {
		var prevState = 'Stopped';

		if ($('#' + id).children('.col-server-state').hasClass('Running')) {
			prevState = 'Running';
		}

		$('#' + id).trigger('setState', 'Loading');

		$('.modal-sage2server').modal('hide');

		var server = {
			id: id,
			name: $('#server-name').val(),
			tag: $('#available-server-versions').val(),
			conf: {
				resolution: {
					width: $('#server-width').val(),
					height: $('#server-height').val(),
				},
				layout: {
					rows: $('#server-rows').val(),
					columns: $('#server-columns').val(),
				},
				ui: {
					clock: 12,
					show_version: true,
					show_url: true,
				},
				background: {
					color: "#333333",
					watermark: {
						svg: "images/EVL-LAVA.svg",
						color: "rgba(255, 255, 255, 0.5)"
					}
				},
				alternate_hosts: [
					"127.0.0.1"
				],
				remote_sites: []
			},
			//userId: $('#server-owner option:selected').val(),
			//user: $('#server-owner option:selected').text(),
			timezone: $('#server-timezone').val()

		};

		$.ajax({
			url: '/container/updateConf',
			type: 'POST',
			data: { server },
			success: function (data) {
				$('#' + id + ' > .col-sm-1 > .server-tag').text(server.tag);
				$('#' + id).trigger('setState', prevState);
			},
			error: function (jqXHR) {
				console.log(jqXHR.responseText + ' :: ' + jqXHR.statusText);
			}
		});
	};
}

