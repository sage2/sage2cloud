// SAGE2 is available for use under the SAGE2 Software License
//
// University of Illinois at Chicago's Electronic Visualization Laboratory (EVL)
// and University of Hawai'i at Manoa's Laboratory for Advanced Visualization
// and Applications (LAVA)
//
// See full text, terms and conditions in the LICENSE.txt included file
//
// Copyright (c) 2017

function DockerController () {

	$('.add-docker-manager').click(function (e) {
		var url = $('.docker-manager-url').val();
		$.ajax({
			url: '/dockerManager/add',
			type: 'POST',
			data: { url: url},
			success: function (data) {

			},
			error: function (jqXHR) {
				console.log(jqXHR.responseText + ' :: ' + jqXHR.statusText);
			}
		});
	});

	$('.delete-docker-manager').click(function (e) {
		var _this = this;
		$('.row-' + _this.id).remove();

		$.ajax({
			url: '/dockerManager/delete',
			type: 'POST',
			data: { id: _this.id },
			success: function(data) {

			},
			error: function(jqXHR) {
				console.log(jqXHR.responseText + ' :: ' + jqXHR.statusText);
			}
		});
	});
}
