// SAGE2 is available for use under the SAGE2 Software License
//
// University of Illinois at Chicago's Electronic Visualization Laboratory (EVL)
// and University of Hawai'i at Manoa's Laboratory for Advanced Visualization
// and Applications (LAVA)
//
// See full text, terms and conditions in the LICENSE.txt included file
//
// Copyright (c) 2017

function SignupController () {
	// redirect to homepage when cancel button is clicked
	$('#account-form-btn1').click(function () {
		window.location.href = '/';
	});

	// redirect to homepage on new account creation,
	// add short delay so user can read alert window
	$('.modal-alert #ok').click(function () {
		setTimeout(function () {
			window.location.href = '/';
		}, 300);
	});
}
