// SAGE2 is available for use under the SAGE2 Software License
//
// University of Illinois at Chicago's Electronic Visualization Laboratory (EVL)
// and University of Hawai'i at Manoa's Laboratory for Advanced Visualization
// and Applications (LAVA)
//
// See full text, terms and conditions in the LICENSE.txt included file
//
// Copyright (c) 2017

function LoginController () {
	// bind event listeners to button clicks
	$('#retrieve-password-submit').click(function () {
		$('#get-credentials-form').submit();
	});

	$('#login #forgot-password').click(function () {
		$('#cancel').html('Cancel');
		$('#retrieve-password-submit').show();
		$('#get-credentials').modal('show');
	});

	$('#login .button-rememember-me').click(function (e) {
		var span = $(this).find('span');
		if (span.hasClass('glyphicon-unchecked')) {
			span.addClass('glyphicon-ok');
			span.removeClass('glyphicon-unchecked');
		}	else {
			span.removeClass('glyphicon-ok');
			span.addClass('glyphicon-unchecked');
		}
	});

	// automatically toggle focus between the email modal window and the login form
	$('#get-credentials').on('shown.bs.modal', function () {
		$('#email-tf').focus();
	});

	$('#get-credentials').on('hidden.bs.modal', function () {
		$('#user-tf').focus();
	});
}
