// SAGE2 is available for use under the SAGE2 Software License
//
// University of Illinois at Chicago's Electronic Visualization Laboratory (EVL)
// and University of Hawai'i at Manoa's Laboratory for Advanced Visualization
// and Applications (LAVA)
//
// See full text, terms and conditions in the LICENSE.txt included file
//
// Copyright (c) 2017

function ContainerController () {
	// bind event listeners to button clicks
	// var _this = this;

	var states = ['Stopped', 'Offline', 'Running', 'Loading'];

	$(document).on('click', '.btn-start-container', function(e) {
		sendCommand(e, 'startServer', 'Running');
	});

	$(document).on('click', '.btn-stop-container', function(e) {
		sendCommand(e, 'stopServer', 'Stopped');
	});

	$(document).on('click', '.btn-configure-container', function(e) {
		var id = $(e.target).closest('.row-container').attr('id');


		$.ajax({
			url: '/fetchServer',
			type: 'POST',
			data: { id: id },
			success: function (server) {
				var timezone = server.timezone === undefined ? 'America/Chicago' : server.timezone;
				$('.modal-sage2server').removeClass('new-sage2server');
				$('.modal-sage2server').addClass('configure-sage2server');
				$('.modal-sage2server').attr('id', id);
				$('.modal-sage2server').modal('show');
				$('#server-name').val(server.name);
				$('#server-columns').val(server.conf.layout.columns);
				$('#server-rows').val(server.conf.layout.rows);
				$('#server-width').val(server.conf.resolution.width);
				$('#server-height').val(server.conf.resolution.height);
				$('#available-server-versions').val(server.tag);
				$('#server-timezone').val(timezone);
				$('#server-owner').val(server.ownerId);
			},
			error: function (jqXHR) {
				console.log(jqXHR.responseText + ' :: ' + jqXHR.statusText);
			}
		});

	});

	$(document).on('click', '.btn-delete-container', function(e) {
		sendCommand(e, 'deleteServer');
	});

	$(document).on('click', '.btn-update-container', function(e) {
		var id = $(e.target).closest('.row-container').attr('id');
		socket.emit('containerLoading', {id: id, status: 'Loading'});

		$.ajax({
			url: '/container/update',
			type: 'POST',
			data: { id: id },
			success: function (server) {
				$('#' + id).removeClass('update');
				$('#' + id).closest('.row-container').addClass('no-update');
			},
			error: function (jqXHR) {
				console.log(jqXHR.responseText + ' :: ' + jqXHR.statusText);
			}
		});
	});

	$('.row-container').on('setState', function(e, state) {
		states.forEach(function(state) {
			$(e.target).children('.col-server-state').removeClass(state);
		});
		$(e.target).children('.col-server-state').addClass(state);
	});

	$(window).resize(function() {
		if ($(window).width() < 1200 && $('div#data').hasClass('table-responsive')) {
			$('div#data').removeClass('table-responsive').addClass('mobile-table-responsive');
		} else if($(window).width() >= 1200 && $('div#data').hasClass('mobile-table-responsive')) {
			$('div#data').removeClass('mobile-table-responsive').addClass('table-responsive');
		}
	});

	var socket = io('/docker');
	socket.on('connect', function() {
		socket.emit('registerClient', $('#userId').val());
	});

	socket.on('containerUpdate', function (o) {
		var row = $('#' + o.id);
		setState(row, o.status);
	});

	socket.on('containerDelete', function(o) {
		var row = $('#' + o.id);
		row.remove();
	});

	socket.on('containerAdd', function(o) {
		$('.container-list').append(o.row);
	});

	socket.on('containerUpdateConf', function(o) {
		$('#' + o.id).replaceWith(o.row);
	});

	// XXX - move this into another place
	socket.on('removeAccount', function(o) {
		if($('.row-' + o.id)) {
			$('.row-' + o.id).remove();
		}
	});

	socket.on('addDockerManager', function(o) {
		$('.row-input').before(o.row);
	});

	socket.on('removeDockerManager', function(o) {
		var row = $('#' + o.id);
		row.remove();
	});

	var sendCommand = function(e, command, state) {
		var id = $(e.target).closest('.row-container').attr('id');
		socket.emit('containerLoading', {id: id, status: 'Loading'});
		$.ajax({
			url: '/home',
			type: 'POST',
			data: { action: command, serverId: id },
			success: function (data) {
				if (command === 'deleteServer') {
				} else {
					getServerStatus(id, function(error, state) {
						$(e.target).closest('.col-server-state').removeClass('update');
						$(e.target).closest('.col-server-state').addClass('no-update');
					});
				}
			},
			error: function (jqXHR) {
				console.log(jqXHR.responseText + ' :: ' + jqXHR.statusText);
			}
		});
	};

	var getServerStatus = function(id, callback) {
		$.ajax({
			url: '/container/checkStatus',
			type: 'POST',
			data: {serverId: id},
			success: function(server) {
				callback(null, server.state);
			},
			error: function(jqXHR) {
				console.log(jqXHR.responseText + ' :: ' + jqXHR.statusText);
				callback(jqXHR);
			}
		});
	};

	var setState = function(e, state) {
		states.forEach(function(state) {
			e.removeClass(state);
		});
		e.addClass(state);
	};
}
