// SAGE2 is available for use under the SAGE2 Software License
//
// University of Illinois at Chicago's Electronic Visualization Laboratory (EVL)
// and University of Hawai'i at Manoa's Laboratory for Advanced Visualization
// and Applications (LAVA)
//
// See full text, terms and conditions in the LICENSE.txt included file
//
// Copyright (c) 2017

function ServerValidator () {
	this.formFields = [$('#server-name'), $('#server-width'), $('#server-height'), $('#server-rows'), $('#server-columns')];
	this.controlGroups = [$('#name-cg'), $('#width-cg'), $('#height-cg'), $('#rows-cg'), $('#columns-cg')];

	this.alert = $('.modal-form-errors');
	this.alert.modal({ show: false, keyboard: true, backdrop: true});
	this.alert.css('z-index', 100000);

	this.validateName = function (s) {
		var reg = /^[\w]+$/;
		if (s.length < 3)			{
			return 'Name too short';
		}		else if (s.indexOf(' ') >= 0)			{
			return 'Name cannot have white space';
		}		else if (!reg.test(s))			{
			return 'Name can only have letters or numbers';
		}		else {
			$.ajax({
				url: '/checkServerName',
				type: 'POST',
				data: { name: $('#server-name').val() },
				success: function (data) {
				},
				error: function (data) {
					$('.name-error').html('Name already taken');
					$('.name-error').show();
					$('#new-server-submit').prop('disabled', true);
				}
			});
		}

		return 'Success';
	};

	this.validateNumber = function (o) {
		var reg = /^[0-9]+$/;
		if (!reg.test(o.value))			{
			return o.value + ' is not a number';
		}

		return 'Success';
	};
}

ServerValidator.prototype.showInvalidField = function (val, target) {
	var field = '.' + target.id.replace(/server-/gi, 'error-');
	$(field).html(val);
	$(field).show();
	$('#new-server-submit').prop('disabled', true);
};

ServerValidator.prototype.showInvalidName = function (val) {
	$('.name-error').html(val);
	$('.name-error').show();
	$('#new-server-submit').prop('disabled', true);
};
