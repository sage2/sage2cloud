// SAGE2 is available for use under the SAGE2 Software License
//
// University of Illinois at Chicago's Electronic Visualization Laboratory (EVL)
// and University of Hawai'i at Manoa's Laboratory for Advanced Visualization
// and Applications (LAVA)
//
// See full text, terms and conditions in the LICENSE.txt included file
//
// Copyright (c) 2017

function LoginValidator () {
	// bind a simple alert window to this controller to display any errors
	this.loginErrors = $('.modal-alert');

	this.showLoginError = function (t, m)	{
		$('.modal-alert .modal-header h4').text(t);
		$('.modal-alert .modal-body').html(m);
		this.loginErrors.modal('show');
	};
}

LoginValidator.prototype.validateForm = function () {
	if ($('#user-tf').val() == '') {
		this.showLoginError('Whoops!', 'Please enter a valid username');
		return false;
	} else if ($('#pass-tf').val() == '') {
		this.showLoginError('Whoops!', 'Please enter a valid password');
		return false;
	} else {
		return true;
	}
};
