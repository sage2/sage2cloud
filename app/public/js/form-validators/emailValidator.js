// SAGE2 is available for use under the SAGE2 Software License
//
// University of Illinois at Chicago's Electronic Visualization Laboratory (EVL)
// and University of Hawai'i at Manoa's Laboratory for Advanced Visualization
// and Applications (LAVA)
//
// See full text, terms and conditions in the LICENSE.txt included file
//
// Copyright (c) 2017

function EmailValidator () {
	// bind this to _local for anonymous functions
	var _this = this;

	// modal window to allow users to request credentials by email
	_this.retrievePassword = $('#get-credentials');
	_this.retrievePasswordAlert = $('#get-credentials .alert');
	_this.retrievePassword.on('show.bs.modal', function () {
		$('#get-credentials-form').resetForm(); _this.retrievePasswordAlert.hide();
	});
}

EmailValidator.prototype.validateEmail = function (e) {
	/* eslint-disable max-len */
	var re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	/* eslint-enable max-len */
	return re.test(e);
};

EmailValidator.prototype.showEmailAlert = function (m) {
	this.retrievePasswordAlert.attr('class', 'alert alert-danger');
	this.retrievePasswordAlert.html(m);
	this.retrievePasswordAlert.show();
};

EmailValidator.prototype.hideEmailAlert = function () {
	this.retrievePasswordAlert.hide();
};

EmailValidator.prototype.showEmailSuccess = function (m) {
	this.retrievePasswordAlert.attr('class', 'alert alert-success');
	this.retrievePasswordAlert.html(m);
	this.retrievePasswordAlert.fadeIn(500);
};
