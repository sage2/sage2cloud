// SAGE2 is available for use under the SAGE2 Software License
//
// University of Illinois at Chicago's Electronic Visualization Laboratory (EVL)
// and University of Hawai'i at Manoa's Laboratory for Advanced Visualization
// and Applications (LAVA)
//
// See full text, terms and conditions in the LICENSE.txt included file
//
// Copyright (c) 2017

'use strict';

var async		= require('async'),
	assert		= require('chai').assert,
	expect		= require('chai').expect,
	mongoose	= require('mongoose'),
	DM			= require('../app/server/modules/docker-manager');

describe('docker-manager.js', function () {
	describe('Testing docker server functions', function () {
		before(function (done) {
			mongoose.connect('mongodb://localhost:27017/sage2CloudTest', done);
		});

		beforeEach(function (done) {
			DM.addDockerManager({url: 'www.bing.com'}, done);
		});

		afterEach(function (done) {
			DM.dropDockerManager(done);
		});

		it('docker manager count should be zero', function (done) {
			DM.dropDockerManager(function (error) {
				if (error) {
					// If MongoError: ns not found then it is empty
					if (error === 'MongoError: ns not found') {
						done();
					}	else {
						done(error);
					}
				} else {
					DM.countDockerManager(function (error, count) {
						if (error) {
							done(error);
						}	else {
							expect(count).to.equal(0);
							done();
						}
					});
				}
			});
		});

		it('add a docker manager', function (done) {
			DM.addDockerManager({url: 'www.google.com'}, function (error, o) {
				// If MongoError: ns not found then it is empty
				if (error) {
					done(error);
				}	else {
					expect(o.url).to.equal('www.google.com');
					done();
				}
			});
		});

		describe('.deleteDockerManager(id)', function() {
			it('should delete url from docker managers based on id', function(done) {
				async.waterfall([
					function(callback) {
						DM.getDockerManagers(callback);
					},
					function(dockers, callback) {
						DM.deleteDockerManager(dockers[0]._id, callback);
					},
					function(arg1, callback) {
						DM.getDockerManagers(callback);
					}
				], function(error, dockers) {
					if(error) {
						done(error);
					} else {
						expect(dockers).to.be.empty;
						done();
					}
				});
			});

			it('does not return any error in case id does not exist', function(done) {
				async.waterfall([
					function(callback) {
						DM.getDockerManagers(callback);
					},
					function(dockers, callback) {
						DM.deleteDockerManager(dockers[0]._id, function(error) {
							if(error) {
								callback(error);
							}
							else {
								callback(null, dockers);
							}
						});
					},
					function(dockers, callback) {
						DM.deleteDockerManager(dockers[0]._id, callback);
					},
					function(arg1, callback) {
						DM.getDockerManagers(callback);
					}
				], function(error, dockers) {
					if(error) {
						done(error);
					} else {
						expect(dockers).to.be.empty;
						done();
					}
				});
			});
		});

		describe('.getDockerManagers()', function(){
			it('returns offline as the status of the www.bing.com docker', function(done) {
				DM.getDockerManagers(function (error, dockers) {
					if (error) {
						done(error);
					}	else {
						expect(dockers[0].status).to.equal('Offline');
						done();
					}
				});
			});

			it('returns a docker array of size 1 and first element equal to www.bing.com', function (done) {
				DM.getDockerManagers(function (error, dockers) {
					if (error) {
						done(error);
					}	else {
						expect(dockers).to.have.lengthOf(1);
						expect(dockers[0].url).to.equal('www.bing.com');
						done();
					}
				});
			});
		});

		it('should return empty array', function (done) {
			async.series([
				function (callback) {
					DM.dropDockerManager(callback);
				},
				function (callback) {
					DM.getDockerManagers(callback);
				}
			],
				function (err, results) {
					if (err) {
						done(err);
					}				else {
						expect(results[1]).to.be.empty;
						done();
					}
				});
		});
	});

	describe('Testing container functions', function() {

		//var Docker = require('dockerode');
		//var docker = new Docker();

		describe('.createContainer(server)', function() {
			var server = {
				name:  'sage2',
				user: 'johndoe',
				docker: 'marrinan.evl.uic.edu',
				url: 'https://marrinan.evl.uic.edu',
				conf: {
					port: 9090,
					index_port: 9292,
					resolution: {
						width: 1920,
						height: 1080,
					},
					layout: {
						rows: 1,
						columns: 1,
					},
					ui: {
						clock: 12,
						show_version: true,
						show_url: true,
					},
					background: {
						color: "#333333",
						watermark: {
							svg: "images/EVL-LAVA.svg",
							color: "rgba(255, 255, 255, 0.5)"
						}
					},
					alternate_hosts: [
						"127.0.0.1"
					],
					remote_sites: [],
					host: 'marrinan.evl.uic.edu',
					displays: []
				}
			};

			var containers = {};
			/*
			before(function(done) {
				DM.createContainer(server, function(error, _containers) {
					expect(error).to.be.null;
					containers = _containers;
					done();
				});
			});
*/
			/*
			after(function(done) {

				DM.deleteContainer({
					'containers': containers
				}, done);
			});
			*/


			it('should create a sage2 container', function(done) {
				console.log(server);
				DM.createContainer(server, done);

				//console.log(containers);
				//var container = docker.getContainer(containers.sage2);
				//container.inspect(done);
			//	done();
			});
			/*

			it('should create a data container', function(done){
				var container = docker.getContainer(containers.data);
				container.inspect(done);
			});
*/
			/*
			it('should write configuration file (docker-cfg.json)', function(done) {
				var stream = require('stream');
				var stdout = new stream.Writable({
					write: function(chunk, encoding, next) {
						expect(chunk.toString().replace(/(\r\n|\n|\r)/gm,"")).to.equal(JSON.stringify(server.conf));
						done();
					}
				});

				var sage2Data = 'sage2Data-' + server.user + '-' + server.name;
				docker.run('mvictoras/sage2', ['bash', '-c', 'cat /sage2/config/docker-cfg.json'], stdout,
					{'VolumesFrom': [ sage2Data ], 'AutoRemove': true},
					function(err, data, container) {
					});
			});
			*/

		});
/*
		describe('.deleteContainer({containers)}', function() {

			var server = {
				'name':  'sage2',
				'user': 'johndoe',
				'conf': {
					port: 9090,
					index_port: 9292,
				}
			};

			var containers;

			beforeEach(function(done) {
				async.series([
					function(callback) {
						DM.createContainer(server, function(error, _containers) {
							expect(error).to.be.null;
							containers = _containers;
							callback();
						});
					},
					function(callback) {
						DM.deleteContainer({
							'containers': containers,
							'shouldDeleteData': true
						},
						callback);
					}
				],
				done);
			});

			it('check that sage2 container does not exist', function(done) {
				docker.getContainer(containers.sage2).inspect(function(error, data) {
					expect(data).to.be.null;
					expect(error.statusCode).to.equal(404);
					done();
				});
			});

			it('check that data container does not exist', function(done) {
				docker.getContainer(containers.data).inspect(function(error, data) {
					expect(data).to.be.null;
					expect(error.statusCode).to.equal(404);
					done();
				});
			});
		});

		describe('.deleteContainer({containers, shouldDeleteData = true)}', function() {

			var server = {
				'name':  'sage2',
				'user': 'johndoe',
				'conf': {
					port: 9090,
					index_port: 9292,
				}
			};

			var containers;

			beforeEach(function(done) {
				async.series([
					function(callback) {
						DM.createContainer(server, function(error, _containers) {
							expect(error).to.be.null;
							containers = _containers;
							callback();
						});
					},
					function(callback) {
						DM.deleteContainer({
							'containers': containers,
							'shouldDeleteData': true
						},
						callback);
					}
				],
				done);
			});

			it('check that sage2 container does not exist', function(done) {
				docker.getContainer(containers.sage2).inspect(function(error, data) {
					expect(data).to.be.null;
					expect(error.statusCode).to.equal(404);
					done();
				});
			});

			it('check that data container does not exist', function(done) {
				docker.getContainer(containers.data).inspect(function(error, data) {
					expect(data).to.be.null;
					expect(error.statusCode).to.equal(404);
					done();
				});
			});
		});

		describe('.deleteContainer({containers, shouldDeleteData = false)}', function() {

			var server = {
				'name':  'sage2',
				'user': 'johndoe',
				'conf': {
					port: 9090,
					index_port: 9292,
				}
			};

			var containers;

			beforeEach(function(done) {
				async.series([
					function(callback) {
						DM.createContainer(server, function(error, _containers) {
							expect(error).to.be.null;
							containers = _containers;
							callback();
						});
					},
					function(callback) {
						DM.deleteContainer({
						'containers': containers,
						'shouldDeleteData': false
						}, callback);
					}
				],
				done);
			});

			afterEach(function(done) {
				docker.getContainer(containers.data).remove(done);
			});

			it('check that sage2 container does not exist', function(done) {
				docker.getContainer(containers.sage2).inspect(function(error, data) {
					expect(data).to.be.null;
					expect(error.statusCode).to.equal(404);
					done();
				});
			});

			it('check that data container does exist', function(done) {
				docker.getContainer(containers.data).inspect(function(error, data) {
					expect(error).to.be.null;
					expect(data.Id).to.equal(containers.data);
					done();
				});
			});
		});
		*/
	});


	after(function (done) {
		mongoose.connection.db.dropDatabase(function () {
			mongoose.connection.close(done);
		});
	});
});
