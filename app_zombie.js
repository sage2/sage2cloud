// SAGE2 is available for use under the SAGE2 Software License
//
// University of Illinois at Chicago's Electronic Visualization Laboratory (EVL)
// and University of Hawai'i at Manoa's Laboratory for Advanced Visualization
// and Applications (LAVA)
//
// See full text, terms and conditions in the LICENSE.txt included file
//
// Copyright (c) 2017

// Node.js Login Boilerplate
// More Info : http://kitchen.braitsch.io/building-a-login-system-in-node-js-and-mongodb/
// Copyright (c) 2013-2016 Stephen Braitsch

var http			= require('http')
var https = require('https')
var express = require('express')
var session = require('express-session')
var bodyParser = require('body-parser')
var errorHandler = require('errorhandler')
var cookieParser = require('cookie-parser')
var mongoose = require('mongoose')
var MongoStore = require('connect-mongo')(session)
var fs				= require('fs')

var app = express()

app.locals.pretty = true
app.set('port', 10000)
app.set('views', __dirname + '/app/server/views')
app.set('view engine', 'pug')
app.use(cookieParser())
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))
app.use(require('stylus').middleware({ src: __dirname + '/app/public' }))
app.use(express.static(__dirname + '/app/public'))

// build mongo database connection url

var dbHost = 'localhost'
var dbPort = 27017
var dbName = 'sage2CloudZombie'

app.use(session({
  secret: 'faeb4453e5d14fe6f6d04637f78077c76c73d1b4',
  proxy: true,
  resave: true,
  saveUninitialized: true,
  store: new MongoStore({ mongooseConnection: mongoose.connection })
})
)

require('./app/server/routes')(app)

http.createServer(app).listen(10000, function () {
  console.log('Server listening on port ' + app.get('port'))
})
