// SAGE2 is available for use under the SAGE2 Software License
//
// University of Illinois at Chicago's Electronic Visualization Laboratory (EVL)
// and University of Hawai'i at Manoa's Laboratory for Advanced Visualization
// and Applications (LAVA)
//
// See full text, terms and conditions in the LICENSE.txt included file
//
// Copyright (c) 2017

'use strict';

var async		= require('async'),
	mongoose = require('mongoose'),
	expect		= require('chai').expect,
	assert		= require('chai').assert,
	SM			= require('../app/server/modules/server-manager'),
	AM			= require('../app/server/modules/account-manager'),
	SC			= require('../app/server/modules/server-controller');

describe('Testing Account Manager:', function () {
	before(function (done) {
		mongoose.connect('mongodb://localhost:27017/sage2CloudTest', done);
	});

	it('account count should be zero', function (done) {
		// Delete all records
		AM.dropAccounts(function (error) {
			if (error) {
				// If MongoError: ns not found then it is empty
				if (error === 'MongoError: ns not found') {
					done();
				}	else {
					done(error);
				}
			} else {
				AM.countAccounts(function (error, count) {
					if (error) {
						done(error);
					}	else {
						expect(count).to.equal(0);
						done();
					}
				});
			}
		});
	});

	it('adding a new user', function (done) {
		var user = {
			name: 'John Doe',
			email: 'john@doe.com',
			user: 'johndoe',
			password: '1234'
		};
		AM.addNewAccount(user, function (error) {
			if (error) {
				done(error);
			}	else {
				done();
			}
		});
	});

	it('should find the correct user and must not be admin', function (done) {
		var user = {
			name: 'Jane Doe',
			email: 'jane@doe.com',
			user: 'janedoe',
			password: '1234'
		};

		AM.addNewAccount(user, function (error, newAccount) {
			if (error) {
				done(error);
			}	else {
				AM.getAccountByUser('janedoe', function (account) {
					expect(account._id.toString()).to.equal(newAccount._id.toString());
					expect(account.admin).to.equal(false);
					done();
				});
			}
		});
	});

	it('should find the correct user and must be admin', function (done) {
		AM.getAccountByUser('johndoe', function (account) {
			expect(account.admin).to.equal(true);
			done();
		});
	});

	it('should return username-taken', function (done) {
		var user = {
			name: 'John Doe',
			email: 'john@doe.com',
			user: 'johndoe',
			password: '1234'
		};
		AM.addNewAccount(user, function (error) {
			if (error) {
				expect(error).to.equal('username-taken');
				done();
			} else {
				done('did not return username-taken');
			}
		});
	});

	it('should return email-taken', function (done) {
		var user = {
			name: 'John Doe',
			email: 'john@doe.com',
			user: 'johndoe2',
			password: '1234'
		};
		AM.addNewAccount(user, function (error) {
			if (error) {
				expect(error).to.equal('email-taken');
				done();
			} else {
				done('did not return email-taken');
			}
		});
	});

	it('should return empty array', function (done) {
		async.series([
			function (callback) {
				AM.dropAccounts(callback);
			},
			function (callback) {
				AM.getAllAccounts(callback);
			}
		],
			function (err, results) {
				if (err) {
					done(err);
				}				else {
					expect(results[1]).to.be.empty;
					done();
				}
			});
	});

	after(function (done) {
		mongoose.connection.db.dropDatabase(function () {
			mongoose.connection.close(done);
		});
	});
});
