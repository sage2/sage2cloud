// SAGE2 is available for use under the SAGE2 Software License
//
// University of Illinois at Chicago's Electronic Visualization Laboratory (EVL)
// and University of Hawai'i at Manoa's Laboratory for Advanced Visualization
// and Applications (LAVA)
//
// See full text, terms and conditions in the LICENSE.txt included file
//
// Copyright (c) 2017

'use strict';

require('chai').expect;
require('chai').assert;

const Browser = require('zombie');

process.env.NODE_ENV = 'test';
// get the application server module
require('../app_zombie');

// We're going to make requests to http://example.com/signup
// Which will be routed to our test server localhost:3000
Browser.localhost('example.com', 10000);

describe('User visits signup page', function () {
	const browser = new Browser();

	before(function () {
		return browser.visit('/signup');
	});

	describe('submits form', function () {
		before(function () {
			browser
				.fill('name',    	'John Doe')
				.fill('email', 		'johndoe@mail.com')
				.fill('user',		'johndoe')
				.fill('pass',		'12345john!');
			return browser.pressButton('Submit');
		});

		it('should be successful', function () {
			browser.assert.success();
		});

		it('should see Account Created!', function () {
			browser.assert.text('.modal-alert .modal-header h4', 'Account Created!');
		});
	});
});

describe('User logs in', function () {
	const browser = new Browser();

	before(function () {
		return browser.visit('/');
	});

	describe('attemmpts to log in', function () {
		before(function () {
			browser
				.fill('user', 	'johndoe')
				.fill('pass', 	'12345john!');
			return browser.pressButton('Sign In');
		});

		it('should be successful', function () {
			browser.assert.success();
		});

/*
		it('should see title', function() {
			browser.assert.text('.navbar-brand', 'SAGE2 Management Portal');
		});

		it('should see SAGE2 Instances Panel heading', function() {
			browser.assert.text('.panel-primary .panel-heading', 'SAGE2 Instances');
		});

		it('SAGE2 server list should be empty', function() {
			browser.assert.text('.panel-body .alert-danger', 'No SAGE2 Servers found. Please add one by clicking the button below.');
		});
*/
		// FIX
		/*
		describe('check menu', function() {
			it('should have 4 menu items', function() {
				browser.assert.elements('.navbar-nav li', 4);
			});
		});

		it('check cookie', function() {
			const value = browser.getCookie('user');
			console.log('Cookie', value);
		});
		*/
	});
});
