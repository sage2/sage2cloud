// SAGE2 is available for use under the SAGE2 Software License
//
// University of Illinois at Chicago's Electronic Visualization Laboratory (EVL)
// and University of Hawai'i at Manoa's Laboratory for Advanced Visualization
// and Applications (LAVA)
//
// See full text, terms and conditions in the LICENSE.txt included file
//
// Copyright (c) 2017

'use strict';

var assert		= require('chai').assert,
	expect		= require('chai').expect,
	async 		= require('async'),
	mongoose		= require('mongoose'),
	SM			= require('../app/server/modules/server-manager'),
	AM			= require('../app/server/modules/account-manager'),
	DM			= require('../app/server/modules/docker-manager'),
	SC			= require('../app/server/modules/server-controller');

describe('Testing Server Controller', function () {
	var user = {
		name: 'John Doe',
		email: 'john@doe.com',
		user: 'johndoe',
		password: '1234'
	};

	var server = {
		name: 'sage2',
		user: 'johndoe',
		conf: {
			layout: {
				columns: 1,
				rows: 1
			},
			resolution: {
				width: 1920,
				height: 1080
			}
		}
	};

	before(function (done) {
		mongoose.connect('mongodb://localhost:27017/sage2CloudTest', done);
	});

	beforeEach(function (done) {
		async.series([
			function (callback) {
				AM.addNewAccount(user, callback);
			},
			function (callback) {
				SM.addNewServer(server, callback);
			},
			function (callback) {
				DM.addDockerManager({url: 'www.google.com'}, callback);
			}
		],
		function (err, results) {
			done();
		});
	});

	afterEach(function (done) {
		async.series([
			function (callback) {
				SM.dropServers(callback);
			},
			function (callback) {
				AM.dropAccounts(callback);
			},
			function (callback) {
				DM.dropDockerManager(callback);
			}
		],
		function (err, results) {
			done();
		});
	});

	/*
	it('get all servers, users, and dockers', function (done) {
		SC.getAllServersUsersDockers(function (error, data) {
			if (error) {
				done(error);
			}	else {
				expect(data.accounts).to.have.lengthOf(1);
				expect(data.servers).to.have.lengthOf(1);
				expect(data.dockerManagers).to.have.lengthOf(1);
				expect(data.accounts[0].user).to.equal('johndoe');
				expect(data.servers[0].name).to.equal('sage2');
				expect(data.dockerManagers[0].url).to.equal('www.google.com');
				done();
			}
		});
	});

	it('get all servers, users, and dockers: servers empty, should still return dockers', function (done) {
		async.series([
			function (callback) {
				SM.dropServers(callback);
			},
			function (callback) {
				SC.getAllServersUsersDockers(callback);
			}
		],
		function (err, results) {
			if (err) {
				done(error);
			}			else {
				expect(results[1].accounts).to.have.lengthOf(1);
				expect(results[1].servers).to.have.lengthOf(0);
				expect(results[1].dockerManagers).to.have.lengthOf(1);
				expect(results[1].accounts[0].user).to.equal('johndoe');
				expect(results[1].dockerManagers[0].url).to.equal('www.google.com');
				done();
			}
		});
	});

	it('get all servers, users, and dockers: dockers empty, should still return servers', function (done) {
		async.series([
			function (callback) {
				DM.dropDockerManager(callback);
			},
			function (callback) {
				SC.getAllServersUsersDockers(callback);
			}
		],
		function (err, results) {
			if (err) {
				done(error);
			}			else {
				expect(results[1].accounts).to.have.lengthOf(1);
				expect(results[1].servers).to.have.lengthOf(1);
				expect(results[1].accounts[0].user).to.equal('johndoe');
				expect(results[1].servers[0].name).to.equal('sage2');
				done();
			}
		});
	});
	*/
	after(function (done) {
		mongoose.connection.db.dropDatabase(function () {
			mongoose.connection.close(done);
		});
	});
});
