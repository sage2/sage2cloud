// SAGE2 is available for use under the SAGE2 Software License
//
// University of Illinois at Chicago's Electronic Visualization Laboratory (EVL)
// and University of Hawai'i at Manoa's Laboratory for Advanced Visualization
// and Applications (LAVA)
//
// See full text, terms and conditions in the LICENSE.txt included file
//
// Copyright (c) 2017

'use strict';

/*
describe("Testing new-server", function() {

	it("should connect to mongodb", function(done) {
		var MongoDB		= require('mongodb').Db;
		var Server		= require('mongodb').Server;

				var dbName = process.env.DB_NAME || 'node-login';
		var dbHost = process.env.DB_HOST || 'localhost'
		var dbPort = process.env.DB_PORT || 27017;

		var db = new MongoDB(dbName, new Server(dbHost, dbPort, {auto_reconnect: true}), {w: 1});
		db.open(function(e, d){
			if (e) {
				done(e);
			} else {
				if (process.env.NODE_ENV == 'live') {
					db.authenticate(process.env.DB_USER, process.env.DB_PASS, function(e, res) {
						if (e) {
							done(e);
						}
						else {
							done();
						}
					});
				}	else{
					done();
				}
			}
		});

	});
});
*/
