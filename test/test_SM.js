// SAGE2 is available for use under the SAGE2 Software License
//
// University of Illinois at Chicago's Electronic Visualization Laboratory (EVL)
// and University of Hawai'i at Manoa's Laboratory for Advanced Visualization
// and Applications (LAVA)
//
// See full text, terms and conditions in the LICENSE.txt included file
//
// Copyright (c) 2017

'use strict';

var async		= require('async'),
	mongoose = require('mongoose'),
	expect		= require('chai').expect,
	assert		= require('chai').assert,
	SM			= require('../app/server/modules/server-manager'),
	AM			= require('../app/server/modules/account-manager'),
	SC			= require('../app/server/modules/server-controller');

describe('Testing Server Manager', function () {
	var user = {
		name: 'John Doe',
		email: 'john@doe.com',
		user: 'johndoe',
		password: '1234'
	};

	before(function (done) {
		mongoose.connect('mongodb://localhost:27017/sage2CloudTest', done);
	});

	beforeEach(function (done) {
		AM.addNewAccount(user, done);
	});

	afterEach(function (done) {
		AM.dropAccounts(done);
	});

	var server = {
		name: 'sage2',
		user: 'johndoe',
		conf: {
			layout: {
				columns: 1,
				rows: 1
			},
			resolution: {width: 1920, height: 1080}
		}
	};

	it('add a new server', function (done) {
		SM.addNewServer(server, function (error) {
			if (error) {
				done(error);
			}	else				{
				done();
			}
		});
	});

	it('should not allow two servers with the same name under the same account', function (done) {
		var server = {
			name: 'sage2',
			user: 'johndoe',
			conf: {
				layout: {
					columns: 1,
					rows: 1
				},
				resolution: {width: 1920, height: 1080}
			}
		};

		SM.addNewServer(server, function (error) {
			if (error) {
				expect(error).to.equal('servername-taken');
				done();
			} else				{
				done('cannot have two servers with the same name under the same account');
			}
		});
	});

	it('should allow two servers with the same name under different accounts', function (done) {
		var server = {
			name: 'sage2',
			user: 'johndoe2',
			conf: {
				layout: {
					columns: 1,
					rows: 1
				},
				resolution: {width: 1920, height: 1080}
			}
		};

		SM.addNewServer(server, function (error) {
			if (error) {
				done(error);
			}	else				{
				done();
			}
		});
	});

	it('should return incorrect width', function (done) {
		var server = {
			name: 'sage2',
			user: 'johndoe',
			conf: {
				layout: {
					columns: 1,
					rows: 1
				},
				resolution: {width: -1, height: 1080}
			}
		};

		SM.addNewServer(server, function (error) {
			if (error) {
				expect(error).to.equal('incorrect-width');
				done();
			} else {
				done('width accepted was less than zero');
			}
		});
	});

	it('should return incorrect height', function (done) {
		var server = {
			name: 'sage2',
			user: 'johndoe',
			conf: {
				layout: {
					columns: 1,
					rows: 1
				},
				resolution: {width: 1920, height: -1}
			}
		};

		SM.addNewServer(server, function (error) {
			if (error) {
				expect(error).to.equal('incorrect-height');
				done();
			} else {
				done('height accepted was less than zero');
			}
		});
	});

	it('should return incorrect columns', function (done) {
		var server = {
			name: 'sage2',
			user: 'johndoe',
			conf: {
				layout: {
					columns: -1,
					rows: 1
				},
				resolution: {width: 1920, height: 1080}
			}
		};

		SM.addNewServer(server, function (error) {
			if (error) {
				expect(error).to.equal('incorrect-columns');
				done();
			} else {
				done('columns accepted was less than zero');
			}
		});
	});

	it('should return incorrect rows', function (done) {
		var server = {
			name: 'sage2',
			user: 'johndoe',
			conf: {
				layout: {
					columns: 1,
					rows: -1
				},
				resolution: {width: 1920, height: 1080}
			}
		};

		SM.addNewServer(server, function (error) {
			if (error) {
				expect(error).to.equal('incorrect-rows');
				done();
			} else {
				done('rows accepted was less than zero');
			}
		});
	});

	it('server count should be zero', function (done) {
		// Delete all records
		SM.dropServers(function (error) {
			if (error) {
				done(error);
			}	else {
				SM.countServers(function (error, count) {
					if (error) {
						done(error);
					}	else {
						expect(count).to.equal(0);
						done();
					}
				});
			}
		});
	});

	it('should return 1025 for http port', function (done) {
		SM.getNextPort(function (error, port) {
			if (error) {
				done(error);
			}	else {
				expect(port).to.equal(1025);
				done();
			}
		});
	});

	it('should find the correct server by id', function (done) {
		var server = {
			name: 'sage30',
			user: 'johndoe',
			conf: {
				layout: {
					columns: 1,
					rows: 1
				},
				resolution: {width: 1920, height: 1080}
			}
		};

		SM.addNewServer(server, function (error, newServer) {
			if (error) {
				done(error);
			}	else {
				SM.getServerById(newServer._id, function (error, server) {
					expect(server._id.toString()).to.equal(newServer._id.toString());
					done();
				});
			}
		});
	});

	it('should delete server sage3', function (done) {
		var server = {
			name: 'sage3',
			user: 'johndoe',
			conf: {
				layout: {
					columns: 1,
					rows: 1
				},
				resolution: {width: 1920, height: 1080}
			}
		};

		SM.addNewServer(server, function (error, newServer) {
			if (error) {
				done(error);
			}	else {
				SM.deleteServer(newServer._id, function (error, server) {
					if (error) {
						done(error);
					}	else {
						SM.getServerById(newServer._id, function (error, server) {
							assert.isNull(server);
							done();
						});
					}
				});
			}
		});
	});

	it('should return no-servers-found', function (done) {
		SM.dropServers(function (error) {
			if (error) {
				done(error);
			}	else {
				AM.getAccountByUser('johndoe', function (account) {
					SM.getServersByUserId(account._id, function (error, servers) {
						if (error) {
							expect(error).to.equal('no-servers-found');
							done();
						} else {
							done('returned array is not empty');
						}
					});
				});
			}
		});
	});

	it('should return two servers', function (done) {
		// First get the user id
		AM.getAccountByUser('johndoe', function (account) {
			var userId = account._id.toString();
			var server = {
				name: 'sage2',
				user: 'johndoe',
				userId: userId,
				conf: {
					layout: {
						columns: 1,
						rows: 1
					},
					resolution: {width: 1920, height: 1080}
				}
			};

			SM.addNewServer(server, function (error) {
				if (error) {
					done(error);
				}	else {
					var server = {
						name: 'sage3',
						user: 'johndoe',
						userId: userId,
						conf: {
							layout: {
								columns: 1,
								rows: 1
							},
							resolution: {width: 1920, height: 1080}
						}
					};

					SM.addNewServer(server, function (error) {
						if (error) {
							done(error);
						}	else {
							AM.getAccountByUser('johndoe', function (account) {
								if (error) {
									done(error);
								}	else {
									SM.getServersByUserId(account._id, function (error, servers) {
										if (error) {
										done(error);
									}	else {
										expect(servers).to.have.lengthOf(2);
										done();
									}
									});
								}
							});
						}
					});
				}
			});
		});
	});

	it("should delete an account and all it's servers", function (done) {
		AM.getAccountByUser('johndoe', function (account) {
			var userId = account._id.toString();
			SC.deleteAccount(userId, function (error) {
				// Check for user
				AM.getAccountByUser('johndoe', function (_account) {
					if (error) {
						done(error);
					}	else {
						expect(_account).to.not.exist;
						// Check for servers
						SM.getServersByUserId(account._id, function (error, servers) {
							if (error) {
								expect(error).to.equal('no-servers-found');
								done();
							} else {
								done('servers found');
							}
						});
					}
				});
			});
		});
	});

	it('should return empty array', function (done) {
		async.series([
			function (callback) {
				SM.dropServers(callback);
			},
			function (callback) {
				SM.getAllServers(callback);
			}
		],
			function (err, results) {
				if (err) {
					done(err);
				}				else {
					expect(results[1]).to.be.empty;
					done();
				}
			});
	});

	it('should allow two servers with the same name under different accounts', function (done) {
		// XXX - TODO
		done();
	});

	after(function (done) {
		mongoose.connection.db.dropDatabase(function () {
			mongoose.connection.close(done);
		});
	});
});
